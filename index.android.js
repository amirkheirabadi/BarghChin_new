import {
	AppRegistry,
	NetInfo
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Main from './src/main';

function handleFirstConnectivityChange(isConnected) {
	if(!isConnected) {
		Actions.Offline();
	}
}

NetInfo.isConnected.fetch().then(isConnected => {
	if(!isConnected) {
		Actions.Offline();
	}
});

NetInfo.isConnected.addEventListener(
	'change',
	handleFirstConnectivityChange
);
AppRegistry.registerComponent('BarghChinMobile', () => Main);
