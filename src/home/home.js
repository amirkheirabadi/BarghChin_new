import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Menu from '../common/menu';
import Api from '../helper/api';

import {
    Container,
    Content,
    Button,
    Grid,
    Row,
    Col,
    Drawer
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {
    StyleSheet,
    Text,
    View,
    BackAndroid,
    Dimensions,
    Linking
} from 'react-native';
var Styles = require('../style.js');

import HomePage from '../home/homePage';
import HomeCategory from '../home/homeCategory';
import HomeBusiness from '../home/homeBusiness';

var homeTab,
    homeProduct;
export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            exitTimer: 0,
            user: ''
        };

    }

    async componentDidMount() {
        let user = await Api.userFetch();
        if (user) {
            this.setState({user: JSON.parse(user)});
        }
    }

    async fetch() {

        let response = await Api.request('home',
            'POST', {}
        );
        if (response) {
            // homeTab.updateData(response[1]['data']['home']);
            this.setState({response: response[1]});
            // response = response[1];
        }

    }

    closeDrawer = () => {
        this
            .drawer
            .close()
    };

    openDrawer = () => {
        this
            ._drawer
            .open()
    };

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Drawer ref={(ref) => this._drawer = ref} type={'overlay'} openDrawerOffset={(width / 2) - 50} captureGestures={true} panThreshold={.25} content={<Menu />} styles={{
                    drawer: {
                        shadowColor: 'black',
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        elevation: 20
                    },
                    mainOverlay: {
                        backgroundColor: '#000',
                        opacity: 0,
                        width: width,
                        height: height
                    }
                }} tweenDuration={200} acceptTap={true} acceptPan={true} tapToClose={true} negotiatePan={true} tweenHandler={(ratio) => ({
                    mainOverlay: {
                        opacity: ratio / 2
                    }
                })} side='right'>
                <Container>
                    <View style={Styles.headerSection}>
                        <Grid>
                            <Col style={[
                                    Styles.pullLeft, {
                                        paddingLeft: 10,
                                        paddingTop: 5
                                    }
                                ]}>
                                {
                                    this.state.user
                                        ? (
                                            <Button style={[{
                                                        paddingTop: -5
                                                    }
                                                ]} transparent="transparent">
                                                <Icon name='textsms' style={{
                                                        fontSize: 30,
                                                        color: '#fff'
                                                    }}/>
                                            </Button>
                                        )
                                        : (
                                            <View></View>
                                        )
                                }
                            </Col>
                            <Col style={[Styles.pullRight]}>
                                <Text style={[
                                        Styles.fontSans, {
                                            paddingRight: 15,
                                            color: '#fff',
                                            fontSize: 15
                                        }
                                    ]}>برق چین</Text>
                                <Button style={[{
                                            paddingTop: -5
                                        }
                                    ]} transparent="transparent" onPress={this.openDrawer}>
                                    <Icon name='menu' style={{
                                            fontSize: 30,
                                            color: '#fff'
                                        }}/>
                                </Button>
                            </Col>
                        </Grid>
                    </View>
                    <Content>
                      
                    </Content>
                </Container>
                <Button style={[{
                            backgroundColor: '#5c4b8c',
                            position: 'absolute',
                            shadowColor: '#2f4050',
                            left: 20,
                            shadowOpacity: 10,
                            shadowOffset: {
                                width: 30,
                                height: 30
                            },
                            shadowRadius: 50,
                            top: height - 100,
                            paddingTop: 5,
                            borderRadius: 50,
                            height: 50,
                            width: 50
                        }
                    ]}>
                    <Icon name='search' style={{
                            fontSize: 32,
                            color: '#fff'
                        }}/>
                </Button>

            </Drawer>
        );
    }
}