import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    Body
} from 'native-base';

import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import SliderModule from '../modules/sliderModule';
import ProductHorizonta from '../modules/productHorizontal';
import ProductList from '../modules/productList';


import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView,
    Linking
} from 'react-native';
import Api from '../helper/api';

var Styles = require('../style.js');
export default class HomeCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                slider: [
                    {}
                ],
                popular_product: [],
                last_product: []
            }
        };
    }

    updateData(data) {
        this.setState({data: data});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={[Styles.mainWrapper]}>
                    <ScrollView style={[Styles.container]}>
                        <SliderModule data={this.state.data.slider}/>
                        <ProductHorizonta data={this.state.data.popular_product} title="پر بازدیدترین ترین محصولات هفته"/>
                        <ProductList data={this.state.data.popular_product} title="پر بازدیدترین ترین محصولات هفته"/>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}
