import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {Image, Text, View, Dimensions, Animated, TouchableWithoutFeedback,Modal,Picker} from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product : '',
            categories: [],
            category: '',
            productName: '',
        };
    }

    async componentDidMount() {
        let response = await Api.request('categories', 'POST', {
            type: 'product'
        });
        console.log(response);
        if (response[0] == 200) {
            this.setState({
                categories: response[1]['data']
            });
        }
    }

    async _onFetch(name, page = 1, callback, options) {
        var data = {
            name: this.state.productName,
        };

        if (this.state.category) {
            data['category'] = this.state.category;
        }
        let response = await Api.request('products', 'POST', data);

        if (response[0] == 200) {
            return callback([response[1]['data']])
        }
    }

    _renderRowView(rowData) {
        return (
            <View>
                {rowData.map((s, i) => {
                return <ListItem>
                    <TouchableWithoutFeedback  onPress={() => this._onPress(s)}>
                       <View>
                         <Text>{s.name}</Text>
                       </View>
                    </TouchableWithoutFeedback>
                    </ListItem>
                })}
            </View>
        );
    }

    _onPress(rowData) {
        this.setState({
            product:rowData.id
        });

        Actions.pop({
          refresh: {brand: {
            product: rowData.id,
          }}
        });
     }

    _refresh() {
        this.refs.listview._refresh();
    }

    _emptyDisplay() {
        return (
            <View style={[{
                    marginTop: 10,
                    padding:10,
                    alignSelf: 'center',
                }
            ]}>
            <Text style={[styles.fontSans,{textAlign:'center'}]}>
              نتیجه ای برای نمایش وجود ندارد .
            </Text>
            </View>
        );
    }
       
    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                
                <Content theme={barghtheme}>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            Actions.pop();
                        }} iconLeft transparent>
                            <Icon name='chevron-left'/>
                            <Text style={[styles.fontSans]}>بازگشت</Text>
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>انتخاب دسته بندی</Title>
                    </Header>

                    <View style={[{
                            width: width * 0.97,
                            alignSelf: 'center',
                        }
                    ]}>
                    <Row>
                       <Col >
                            <Picker mode="dropdown" selectedValue={this.state.category} onValueChange={(value) => {
                                this.setState({category: value});
                            }} style={[{
                                    paddingRight:15,
                                    paddingTop:30,
                                }
                            ]}>
                                {this.state.categories.map((s, i) => {
                                    return <item value={s.id} label={s.name}/>
                                })}
                            </Picker>
                        </Col>
                    </Row>

                    <InputGroup style={[{marginTop:10}]}>
                        <Button transparent onPress={() => {this._refresh()}}>
                            <Icon name='search' />
                        </Button>
                        <Input placeholder="جستجوی نام محصول ..." onChangeText={(text) => {
                            this.setState({
                                productName: text
                            });
                        }}/>
                    </InputGroup>
                    </View>
                    <Text style={[
                        styles.fontSans,styles.wrapperHeader, {
                            marginTop: 20,
                        }]}>نتایج جستجو :</Text>

                        <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center',
                                backgroundColor: 'rgb(245, 245, 245)'
                            }
                        ]}>
                            <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                                <List style={[{
                                        padding: 0
                                    }
                                ]}>

                    <View style={styles.container}>
                        <View style={styles.navBar}/>
                        <GiftedListView enableEmptySections={true} ref="listview" emptyView={this._emptyDisplay} 
                        rowView={this._renderRowView.bind(this)} 
                        onFetch={this._onFetch.bind(this, this.state.name)} 
                        firstLoader={true} // display a loader for the first fetching
                            pagination={false} // enable infinite scrolling using touch to load more
                            withSections={false} // enable sections
                            customStyles={{
                            paginationView: {
                                backgroundColor: '#eee'
                            }
                        }} refreshableTintColor="blue"/>
                    </View>

                        </List>
                    </CardItem>
                </Card>
                </Content>
            </Container>
        );
    }
}
