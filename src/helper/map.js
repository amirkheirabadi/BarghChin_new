import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab
} from 'native-base';
import barghtheme from '../theme/barghchin';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated
} from 'react-native';
import Api from '../helper/api';
import MapView from 'react-native-maps';

var styles = require('../style.js');
export default class Map extends Component {
    constructor(props) {
      super(props);

      this.state = {
          Marker: {
              latitude: 35.696111,
              longitude: 51.423055999999974,
          },

          current : {
            latitude: 35.696111,
            longitude: 51.423055999999974
          }
      }
      if (this.props.geo) {
        this.setState({
          Marker: this.props.geo
        });
      }
    }

    componentDidMount() {
       navigator.geolocation.getCurrentPosition(
         (position) => {
           this.setState({
                current : {
                    latitude : position.coords.latitude,
                    longitude: position.coords.longitude,
                }
            })
         },
         (error) => console.log(JSON.stringify(error)),
         {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000}
       );
     }

    gotToCurrent() {
        this.refs.map.animateToRegion({
            latitude: this.state.current.latitude,
            longitude: this.state.current.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },1000)
    }

    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (
            <Container>
                <Content theme={barghtheme}>
                    <Header style={[{
                            flexDirection: 'row'
                        }
                    ]}>
                        <Button onPress={() => {
                            Actions.pop({
                              refresh: {geo: this.state.Marker}
                            });
                        }} iconLeft transparent>
                            <Icon name='chevron-left'/>
                            <Text>بازگشت</Text>
                        </Button>
                        <Title style={[
                            styles.fontSans, {
                                color: '#fff',
                                textAlign: 'center',
                                alignSelf: 'flex-end',
                                fontSize: 16
                            }
                        ]}>فروشگاه / شرکت</Title>
                    </Header>
                    <MapView
                        ref="map"
                        style={[{width:width,height:height}]}
                        initialRegion = {{
                            latitude: 35.696111,
                            longitude: 51.423055999999974,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                     >
                     <MapView.Marker
                      coordinate={this.state.Marker}
                      draggable
                    />
                 </MapView>
                
                </Content>
                            <Fab active={true}
               direction="right"
               containerStyle={{ marginLeft: 10 }}
               style={{ backgroundColor: '#5c4b8c' }}
               position="bottomLeft"
               onPress={() => this.gotToCurrent()}
               >
               <Icon name="my-location" />
           </Fab>
            </Container>
        );
    }
}

Map.propTypes = {
  provider: MapView.ProviderPropType,
};
