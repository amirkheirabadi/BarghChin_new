import React, { Component } from 'react';

import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Left,
	Right,
	Button,
	Grid,
	Row,
	Col,
	List,
	ListItem,
	Card,
	CardItem,
	Fab,
	Tabs,
	Tab,
	Radio,
	Picker,
	Item,
	CheckBox,
	TouchableWithoutFeedback
} from 'native-base';
import barghtheme from '../theme/barghchin';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Image, Text, View, Dimensions, Animated } from 'react-native';
import Api from '../helper/api';

var Styles = require('../style.js');
export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			current: {
				latitude: '',
				longitude: ''
			},
			search: {
				type: '',
				distance: '',
				city_id: '',
				businessType: '',
				brand: '',
				category: '',
				search: ''
			}
		};
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;

		return <Container>
				<Header style={[Styles.HeaderWrapper]}>
					<Left>
						<Button onPress={() => {
								Actions.Home({ type: 'reset' });
							}} transparent>
							<View style={[Styles.headerActionWrap]}>
								<Icon name="chevron-left" style={[Styles.headerActionIcon]} />
								<Text
									style={[
										Styles.fontSans,
										Styles.headerActionText
									]}>
									بازگشت
								</Text>
							</View>
						</Button>
					</Left>
					<Right>
						<Title
							style={[
								Styles.fontSans,
								Styles.headerTitleRight
							]}>
							جستجو
						</Title>
					</Right>
				</Header>
				<Content theme={barghtheme} style={[Styles.mainWrapper]} keyboardShouldPersistTaps="always">
					<View >
						<Row>
							<Col>
								<Picker mode="dropdown">
									<Item label="Wallet" value="key0" />
									<Item label="ATM Card" value="key1" />
									<Item label="Debit Card" value="key2" />
									<Item label="Credit Card" value="key3" />
									<Item label="Net Banking" value="key4" />
								</Picker>
							</Col>
							<Col>
								<Item>
									<Input placeholder="نام کسب و کار" />
									<Icon active name="search" />
								</Item>
							</Col>
						</Row>

						<Row>
							<Row style={[{ alignSelf: 'flex-end' }]}>
								<Text style={[Styles.fontSans]}>
									جستجو بر حسب موقعیت فعلی
								</Text>
								<Radio selected={this.state.locationType != 'city'} onPress={() => {
										this.changeType();
									}} style={[{ marginLeft: 10 }]} />
							</Row>
							<Row>
								<Picker style={[{ width: width * 0.5 }]} mode="dropdown" enabled={this.state.locationType == 'distance'} selectedValue={this.state.distance} onValueChange={value => {
										this.setState({
											distance: value
										});
									}}>
									<Item label="۱ کیلومتر" value="1" />
									<Item label="۲ کیلومتر" value="2" />
									<Item label="۵ کیلومتر" value="5" />
									<Item label="۱۰ کیلومتر" value="10" />
								</Picker>
							</Row>
							<Row style={[{ alignSelf: 'flex-end', marginTop: 10 }]}>
								<Text style={[Styles.fontSans]}>
									جستجو بر حسب شهر
								</Text>
								<Radio selected={this.state.locationType == 'city'} onPress={() => {
										this.changeType();
									}} style={[{ marginLeft: 10 }]} />
							</Row>
							<Row style={[{ alignSelf: 'flex-end', marginTop: 10 }]}>
								<Picker mode="dropdown" enabled={this.state.locationType == 'city'} style={[{ width: width * 0.5 }]} />
							</Row>

							<Row style={[{ alignSelf: 'flex-end' }]}>
								<Picker mode="dropdown" enabled={this.state.locationType == 'city'} onValueChange={value => {
										this.setState({
											search: {
												city_id: value
											}
										});
									}} style={[{ width: width * 0.5 }]} />
							</Row>
						</Row>
					</View>

				
				</Content>
			</Container>;
	}
}
