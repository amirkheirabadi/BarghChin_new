import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import ImagePicker from 'react-native-image-picker';
import {Actions} from 'react-native-router-flux';
import {
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback,
    Image,
    ScrollView
} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');
var config = require('../config');
export default class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            price: '',
            thumb: {
                uri: '',
                id: 'thumb'
            },
            category: '',
            gallery: [
                {
                    uri: '',
                    id: ''
                }, {
                    uri: '',
                    id: ''
                }, {
                    uri: '',
                    id: ''
                }, {
                    uri: '',
                    id: ''
                }
            ],
            deleteGallery: [],
            entity: 'company', //this.props.entity,
            productId: '',
            imagePicker: {
                title: 'انتخاب تصویر',
                mediaType: 'photo',
                quality: 1,
                maxWidth: 960,
                maxHeight: 540,
                cancelButtonTitle: 'انصراف',
                takePhotoButtonTitle: 'گرفتن تصویر',
                chooseFromLibraryButtonTitle: 'انتخاب از گالری',
                storageOptions: {
                    skipBackup: true,
                    path: 'images'
                }
            }
        };
    }
    async save() {
        var url;
        if (this.state.productId) {
            url = 'product/update/' +
                    this.state.productId;
        } else {
            url = 'product/create';
        }
        var reqData = {
            name: this.state.name,
            description: this.state.description,
            price: this.state.price,
            category: this.state.category,
            entity: this.state.entity
        };
        var reqFile = {
            pictures: this.state.gallery
        };
        if (this.state.thumb['id'] && this.state.thumb['uri']) {
            reqFile['thumb'] = this
                .state
                .thumb['uri']
        }
        if (this.state.productId) {
            var gallery = [];
            this
                .state
                .gallery
                .forEach(function (picture, index) {
                    if (picture['uri'] && !picture['id']) {
                        gallery.push(picture['uri']);
                    }
                });
            reqData['delete_gallery'] = this.state.deleteGallery;
            reqFile['pictures'] = gallery;
        } else {
            if (!this.state.thumb) {
                Toast.show('لطفا یک تصویر به عنوان پیش نمایش محصول انتخاب کنید .', {
                    duration: Toast.durations.LONG,
                    position: -50,
                    backgroundColor: '#e74c3c',
                    shadow: true,
                    animation: true,
                    hideOnPress: true,
                    delay: 0,
                    textStyle: {
                        color: '#fff'
                    }
                }
                );
                return;
            }
        }
        let status = await Api.request(url,
            'POST',
            reqData,
            reqFile
        );
        if (status[0] == 200) {
            return Actions.Settings();
        }
        Toast.show('خطا در ثبت محصول', {
            duration: Toast.durations.LONG,
            position: -50,
            backgroundColor: '#e74c3c',
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            textStyle: {
                color: '#fff'
            }
        }
        );
    }
    async componentDidMount() {
        if (true) { //
            this.setState({
                productId: 1 //this.props.productId
            });
            let response = await Api.request("product/edit/" +
                    1,
                "POST"
            ); //this.props.productId
            if (response[0] == 200) {
                data = response[1]['data'];
                var gallery = [];
                data['pictures'].forEach(function (picture, index) {
                    gallery.push({id: picture['id'], uri: picture['url']})
                });
                if (gallery.length < this.state.gallery.length) {
                    for (var i = 1; i < this.state.gallery.length; i++) {
                        gallery.push({id: '', uri: ''});
                    }
                }
                this.setState({
                    name: data['name'],
                    price: data['price'],
                    description: data['description'],
                    category: data['category']['name'],
                    thumb: {
                        uri: data['thumb'],
                        id: ''
                    },
                    gallery: gallery
                });
            } else {
                return Actions.pop();
            }
        }
    }
    componentWillReceiveProps(props) {
        if ("category" in props) {
            this.setState({category: props['category']['name']
            });
        }
    }
    imagePicker(index) {
        ImagePicker.showImagePicker(this.state.imagePicker,
            (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ',
                    response.error
                );
            } else if (response.customButton) {
                console.log('User tapped custom button: ',
                    response.customButton
                );
            } else {
                let source = {
                    uri: response.uri
                };
                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                if (index != undefined) {
                    var gallery = this.state.gallery;
                    gallery[index]['uri'] = response.uri;
                    this.setState({gllery: gallery});
                } else {
                    this.setState({
                        thumb: {
                            uri: response.uri,
                            id: 'thumb'
                        }
                    });
                }
            }
        }
        );
    }
    deleteGallery(index) {
        console.log('delete');
        var gallery = this.state.gallery;
        var deleteGallery = this.state.deleteGallery;
        if (gallery[index] != undefined) {
            if (gallery[index]['id']) {
                deleteGallery.push(gallery[index]['id']);
            }
            gallery[index]['uri'] = '';
            gallery[index]['id'] = '';
            this.setState({gallery: gallery, deleteGallery: deleteGallery});
        }
    }
    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={styles.mainWrapper}>
                    <Header style={[{
                                flexDirection: 'row'
                            }
                        ]}>
                        <Button onPress={() => {
                                this.save();
                            }} iconRight="iconRight" transparent="transparent">
                            <Icon name='done'/> {
                                this.state.productId
                                    ? (
                                        <Text>ویرایش</Text>
                                    )
                                    : (
                                        <Text>ایجاد</Text>
                                    )
                            }
                        </Button>
                        <Title style={[
                                styles.fontSans, {
                                    color: '#fff',
                                    textAlign: 'center',
                                    alignSelf: 'flex-end',
                                    fontSize: 16
                                }
                            ]}>{
                                this.state.productId == "create"
                                    ? ("ایجاد محصول")
                                    : ("ویرایش محصول")
                            }</Title>
                    </Header>

                    <Row>
                        <Col style={[{
                                    padding: 20
                                }
                            ]}>
                            <TouchableWithoutFeedback onPress={() => {
                                    this.imagePicker()
                                }}>
                                {
                                    this.state.thumb
                                        ? (
                                            <View style={[{
                                                        backgroundColor: '#95a5a6',
                                                        height: 100,
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }
                                                ]}>
                                                <Image source={{
                                                        uri: config['server'] +
                                                                this.state.thumb.uri +
                                                                '_small.jpg'
                                                    }} style={[{
                                                            height: 100,
                                                            alignSelf: 'stretch',
                                                            textAlign: 'center'
                                                        }
                                                    ]}/>
                                            </View>
                                        )
                                        : (
                                            <View style={[{
                                                        backgroundColor: '#95a5a6',
                                                        height: 100,
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }
                                                ]}>
                                                <Row>
                                                    <Text style={[
                                                            styles.fontSans, {
                                                                alignSelf: 'center',
                                                                color: "#fff"
                                                            }
                                                        ]}>آپلود تصویر</Text>
                                                </Row>
                                                <Row>
                                                    <Icon name="touch-app" style={[{
                                                                color: "#fff"
                                                            }
                                                        ]}></Icon>
                                                </Row>
                                            </View>
                                        )
                                }
                            </TouchableWithoutFeedback>
                        </Col>

                        <Col style={[{
                                    justifyContent: 'flex-end',
                                    alignItems: 'flex-end',
                                    paddingTop: 20,
                                    paddingRight: 20
                                }
                            ]}>
                            <Row>
                                <Text style={[styles.fontSans, {}]}>تصویر محصول</Text>
                            </Row>
                            <Row >
                                <Text style={[
                                        styles.fontSans, {
                                            fontSize: 11
                                        }
                                    ]}>از این تصویر برای نمایش محصول شما در منو ها و لیستهای مختلف اپ و سایت استفاده خواهد شد .</Text>
                            </Row>
                        </Col>
                    </Row>

                    <Text style={[styles.fontSans, styles.wrapperHeader]}>تصاویر گالری</Text>

                    <ScrollView ref={(scrollView) => {
                            _scrollView = scrollView;
                        }} automaticallyAdjustContentInsets={true} scrollEventThrottle={200} horizontal={true} style={styles.scrollView}>
                        {
                            this
                                .state
                                .gallery
                                .map((s,
                                    i
                                ) => {
                                    return <TouchableWithoutFeedback onPress={() => {
                                            this.imagePicker(i)
                                        }}>
                                        {
                                            this
                                                .state
                                                .gallery[i]['uri']
                                                    ? (
                                                        <View style={[{
                                                                    backgroundColor: '#95a5a6',
                                                                    height: 100,
                                                                    width: 200,
                                                                    marginLeft: 20,
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center'
                                                                }
                                                            ]}>
                                                            <Image source={{
                                                                    uri: config['server'] +
                                                                            s.uri
                                                                }} style={[{
                                                                        height: 100,
                                                                        alignSelf: 'stretch',
                                                                        textAlign: 'center'
                                                                    }
                                                                ]}/>

                                                            <View style={[{
                                                                        position: 'absolute',
                                                                        left: 5,
                                                                        top: 5
                                                                    }
                                                                ]}>
                                                                <TouchableWithoutFeedback onPress={() => {
                                                                        this.deleteGallery(i)
                                                                    }}>
                                                                    <View style={[{
                                                                                backgroundColor: '#5c4b8c'
                                                                            }
                                                                        ]}>
                                                                        <Icon name="close" style={[{
                                                                                    color: '#fff'
                                                                                }
                                                                            ]}></Icon>
                                                                    </View>
                                                                </TouchableWithoutFeedback>
                                                            </View>
                                                        </View>
                                                    )
                                                    : (
                                                        <View style={[{
                                                                    backgroundColor: '#95a5a6',
                                                                    height: 100,
                                                                    width: 200,
                                                                    marginLeft: 20,
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center'
                                                                }
                                                            ]}>
                                                            <Row>
                                                                <Text style={[
                                                                        styles.fontSans, {
                                                                            alignSelf: 'center',
                                                                            color: "#fff"
                                                                        }
                                                                    ]}>آپلود تصویر</Text>
                                                            </Row>
                                                            <Row>
                                                                <Icon name="touch-app" style={[{
                                                                            color: "#fff"
                                                                        }
                                                                    ]}></Icon>
                                                            </Row>

                                                        </View>
                                                    )
                                        }
                                    </TouchableWithoutFeedback>
                                })
                        }
                    </ScrollView>

                    <Text style={[
                            styles.fontSans,
                            styles.wrapperHeader, {
                                marginTop: 10
                            }
                        ]}>اطلاعات محصول</Text>
                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <List style={[{
                                        padding: 0
                                    }
                                ]}>
                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <InputGroup borderType='regular' style={[{
                                                borderRadius: 5,
                                                backgroundColor: '#F9FAFB'
                                            }
                                        ]}>
                                        <Input placeholder='نام محصول' value={this.state.name} onChangeText={(text) => {
                                                this.setState({name: text});
                                            }}/>
                                    </InputGroup>
                                </ListItem>

                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <InputGroup borderType='regular' style={[{
                                                borderRadius: 5,
                                                backgroundColor: '#F9FAFB'
                                            }
                                        ]}>
                                        <Input placeholder='قیمت محصول ( ریال )' value={this.state.price} onChangeText={(text) => {
                                                this.setState({price: text});
                                            }}/>
                                    </InputGroup>
                                </ListItem>

                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <InputGroup borderType='regular' style={[{
                                                borderRadius: 5,
                                                backgroundColor: '#F9FAFB'
                                            }
                                        ]}>
                                        <Input style={[{
                                                    height: 100
                                                }
                                            ]} multiline={true} value={this.state.description} placeholder='توضیحات' onChangeText={(text) => {
                                                this.setState({description: text});
                                            }}/>
                                    </InputGroup>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <TouchableWithoutFeedback onPress={() => {
                                    Actions.Categories()
                                }}>
                                <View style={[{
                                            flexDirection: 'row'
                                        }
                                    ]}>
                                    <Col style={[{
                                                padding: 5,
                                                alignItems: 'flex-start'
                                            }
                                        ]}>
                                        <Text style={[
                                                styles.fontSansLight, {
                                                    paddingTop: 3,
                                                    paddingLeft: 15,
                                                    color: '#5c4b8c'
                                                }
                                            ]}>{this.state.category}</Text>
                                    </Col>
                                    <Col style={[{
                                                padding: 5,
                                                alignItems: 'flex-end'
                                            }
                                        ]}>
                                        <View style={[{
                                                    flexDirection: 'row'
                                                }
                                            ]}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        paddingTop: 3,
                                                        paddingRight: 10
                                                    }
                                                ]}>انتخاب دسته بندی</Text>
                                            <Icon name="dashboard" style={{
                                                    color: '#bdc3c7'
                                                }}/>
                                        </View>
                                    </Col>

                                </View>
                            </TouchableWithoutFeedback>
                        </CardItem>
                    </Card>

                </Content>
            </Container>
        );
    }
}
