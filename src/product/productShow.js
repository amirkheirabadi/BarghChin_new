import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import StarRating from 'react-native-star-rating';

import {Actions} from 'react-native-router-flux';
import {
    Text,
    View,
    Dimensions,
    TouchableWithoutFeedback,
    Image,
    ScrollView
} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');
export default class ProductShow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rating: 0,

            name: "",
            comment_count: 0,
            thumb: {
                small: '',
                normal: '',
                large: ''
            },
            category: {
                id: '',
                name: ''
            },
            user_id: '',
            description: "",
            price: "",
            view: 0,
            created: {
                time: '',
                ago: ''
            },
            updated: {
                time: '',
                ago: ''
            },
            productId: '',
            mine: 'no',
            last_product: [],
            related_product: [],
            pictures: [],
            entity: '',
            owner: {
                id: '',
                name: '',
                owner: ''
            }
        };

    }

    async componentDidMount() {
        // if (!this.props.productId) {
        //     return Actions.pop();
        // }
        //  + this.props.productId
        let response = await Api.request('product/show/1',
            'GET'
        );
        if (response[0] != "200") {
            console.log("err");
        }

        var product = response[1]['data'];
        this.setState({
            name: product['product']['name'],
            comment_count: product['product']['comment_count'],
            rating: product['product']['rating'],
            thumb: {
                small: product['product']['thumb']['small'],
                normal: product['product']['thumb']['normal'],
                large: product['product']['thumb']['large']
            },
            category: {
                id: product['product']['category']['id'],
                name: product['product']['category']['name']
            },
            entity: product['product']['entity'],
            description: product['product']['description'],
            price: product['product']['price'],
            view: product['product']['view'],
            created: {
                time: product['product']['created']['time'],
                ago: product['product']['created']['ago']
            },
            updated: {
                time: product['product']['updated']['time'],
                ago: product['product']['updated']['ago']
            },
            productId: product['product']['id'],
            mine: product['product']['mine'],
            last_product: product['last_product'],
            related_product: product['related_product'],
            pictures: product['product']['pictures'],
            owner: {
                id: product['product']['owner']['id'],
                name: product['product']['owner']['name'],
                avatar: product['product']['owner']['avatar']
            }
        });
    }

    componentWillReceiveProps(props) {
        if ("category" in props) {
            this.setState({category: props['category']});
        }
    }

    showGallery() {
        var media = [
            {
                photo: this.state.thumb.large
            }
        ];

        this
            .state
            .pictures
            .forEach(function (picture) {
                media.push({photo: picture.url});
            })

        Actions.Gallery({media: media});
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme} style={[styles.mainWrapper]}>
                    <Row>
                        <TouchableWithoutFeedback onPress={() => {
                                this.showGallery()
                            }}>
                            <Image source={{
                                    uri: this.state.thumb.large
                                }} style={[{
                                        height: height / 2,
                                        width: width,
                                        alignSelf: 'stretch',
                                        textAlign: 'center'
                                    }
                                ]}/>
                        </TouchableWithoutFeedback>
                    </Row>
                    <Text style={[
                            styles.fontSans,
                            styles.wrapperHeader, {
                                fontSize: 16
                            }
                        ]}>{this.state.name}</Text>
                    <Row style={{
                            paddingTop: 5,
                            paddingBottom: 5
                        }}>
                        <Col style={{
                                paddingLeft: 10
                            }}>
                            <Row>
                                <Button bordered="bordered" iconLeft="iconLeft" style={{
                                        borderColor: '##757575',
                                        padding: 0,
                                        borderWidth: 0.3
                                    }} textStyle={{
                                        color: '##757575',
                                        fontSize: 11
                                    }}>
                                    <Icon name='remove-red-eye' style={{
                                            color: '##757575',
                                            fontSize: 16
                                        }}/>
                                    <Text style={[
                                            styles.fontSansLight, {
                                                color: '##757575',
                                                fontSize: 11
                                            }
                                        ]}>{this.state.view}</Text>
                                </Button>
                                <Button bordered="bordered" iconLeft="iconLeft" style={{
                                        borderColor: '##757575',
                                        padding: 0,
                                        marginLeft: 10,
                                        borderWidth: 0.3
                                    }} textStyle={{
                                        color: '##757575',
                                        fontSize: 11
                                    }}>
                                    <Icon name='message' style={{
                                            color: '##757575',
                                            fontSize: 16
                                        }}/>
                                    <Text style={[
                                            styles.fontSansLight, {
                                                color: '##757575',
                                                fontSize: 11
                                            }
                                        ]}>{this.state.comment_count}</Text>
                                </Button>
                            </Row>
                        </Col>

                        <Col>
                            <Row style={{
                                    justifyContent: 'flex-end'
                                }}>
                                <Text style={[
                                        styles.fontSans,
                                        styles.wrapperHeader, {
                                            fontSize: 13
                                        }
                                    ]}>{this.state.price}
                                    ریال</Text>
                                <Text style={[
                                        styles.fontSansLight,
                                        styles.wrapperHeader, {
                                            fontSize: 13
                                        }
                                    ]}>قیمت :
                                </Text>
                            </Row>
                        </Col>
                    </Row>

                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <List style={[{
                                        padding: 0
                                    }
                                ]}>
                                <ListItem style={{
                                        borderBottomWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <ScrollView ref={(scrollView) => {
                                            _scrollView = scrollView;
                                        }} automaticallyAdjustContentInsets={true} scrollEventThrottle={200} horizontal={true} style={[
                                            styles.scrollView, {
                                                paddingTop: 10,
                                                paddingBottom: 10
                                            }
                                        ]}>
                                        {
                                            this
                                                .state
                                                .pictures
                                                .map((s,
                                                    i
                                                ) => {
                                                    return <TouchableWithoutFeedback onPress={() => {
                                                            this.showGallery()
                                                        }}>
                                                        <View style={[{
                                                                    backgroundColor: '#95a5a6',
                                                                    height: 100,
                                                                    width: 150,
                                                                    marginLeft: 20,
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center'
                                                                }
                                                            ]}>
                                                            <Image source={{
                                                                    uri: s.url
                                                                }} style={[{
                                                                        height: 100,
                                                                        alignSelf: 'stretch',
                                                                        textAlign: 'center'
                                                                    }
                                                                ]}/>
                                                        </View>
                                                    </TouchableWithoutFeedback>
                                                })
                                        }
                                    </ScrollView>
                                </ListItem>
                                <ListItem style={{
                                        borderBottomWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <Text style={[
                                            styles.fontSansLight, {
                                                fontSize: 13
                                            }
                                        ]}>توضیحات :</Text>
                                </ListItem>
                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <Text style={[styles.fontSans]}>{this.state.description}</Text>
                                </ListItem>
                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5,
                                        paddingTop: 7
                                    }}>
                                    <Row style={{
                                            justifyContent: 'flex-end'
                                        }}>
                                        <Text style={[
                                                styles.fontSans, {
                                                    fontSize: 13
                                                }
                                            ]}>
                                            {this.state.updated.ago}</Text>
                                        <Text style={[
                                                styles.fontSansLight, {
                                                    fontSize: 13
                                                }
                                            ]}>آخرین بروز رسانی :
                                        </Text>
                                    </Row>
                                </ListItem>

                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5,
                                        paddingTop: 7
                                    }}>
                                    <Col>
                                        <Button transparent="transparent" iconLeft="iconLeft" style={{
                                                borderColor: '#34495e'
                                            }} textStyle={{
                                                color: '#34495e',
                                                fontSize: 11
                                            }}>
                                            <Icon name='keyboard-arrow-left' style={{
                                                    color: '#34495e',
                                                    fontSize: 16
                                                }}/>
                                            <Text style={[
                                                    styles.fontSansLight, {
                                                        color: '#34495e',
                                                        fontSize: 11
                                                    }
                                                ]}>نمایش بیشتر</Text>
                                        </Button>
                                    </Col>

                                    <Col>
                                        <Row style={{
                                                justifyContent: 'flex-end',
                                                paddingTop: 10
                                            }}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        fontSize: 13
                                                    }
                                                ]}>
                                                {this.state.category.name}</Text>
                                            <Text style={[
                                                    styles.fontSansLight, {
                                                        fontSize: 13
                                                    }
                                                ]}>دسته بندی محصول :
                                            </Text>
                                        </Row>
                                    </Col>
                                </ListItem>

                                <ListItem style={{
                                        borderWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <Col>
                                        <Button transparent="transparent" iconLeft="iconLeft" style={{
                                                borderColor: '#34495e',
                                                paddingTop: 15
                                            }} textStyle={{
                                                color: '#34495e',
                                                fontSize: 11
                                            }}>
                                            <Icon name='keyboard-arrow-left' style={{
                                                    color: '#34495e',
                                                    fontSize: 16
                                                }}/>
                                            <Text style={[
                                                    styles.fontSansLight, {
                                                        color: '#34495e',
                                                        fontSize: 11
                                                    }
                                                ]}>اطلاعات بیشتر</Text>
                                        </Button>
                                    </Col>
                                    <Col>
                                        <Row style={{
                                                justifyContent: 'flex-end'
                                            }}>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        fontSize: 13,
                                                        paddingTop: 15
                                                    }
                                                ]}>
                                                {this.state.owner.name}</Text>
                                            {
                                                this.state.entity == "company"
                                                    ? (
                                                        <Text style={[
                                                                styles.fontSansLight, {
                                                                    paddingRight: 10,
                                                                    paddingTop: 15
                                                                }
                                                            ]}>شرکت سازنده :
                                                        </Text>
                                                    )
                                                    : (
                                                        <Text style={[
                                                                styles.fontSansLight, {
                                                                    paddingRight: 10,
                                                                    paddingTop: 15
                                                                }
                                                            ]}>شرکت وارد کننده :
                                                        </Text>
                                                    )
                                            }
                                            <Image style={{
                                                    width: 50,
                                                    height: 50,
                                                    paddingLeft: 10
                                                }} source={{
                                                    uri: this.state.owner.avatar
                                                }}/>
                                        </Row>
                                    </Col>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Text style={[
                            styles.fontSansLight,
                            styles.wrapperHeader, {
                                fontSize: 13
                            }
                        ]}>نظرات کاربری</Text>
                    <Card style={[{
                                width: width * 0.97,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>
                            <List style={[{
                                        padding: 0
                                    }
                                ]}>
                                <ListItem style={{
                                        borderBottomWidth: 0,
                                        paddingTop: 7,
                                        paddingRight: 10,
                                        paddingLeft: 10,
                                        paddingBottom: 5
                                    }}>
                                    <Row style={{
                                            padding: 10
                                        }}>
                                        <Col>
                                            <Button transparent="transparent" iconLeft="iconLeft" style={{
                                                    borderColor: '#34495e'
                                                }} textStyle={{
                                                    color: '#34495e',
                                                    paddingRight: 40,
                                                    fontSize: 11,
                                                    borderRightWidth: 0.5
                                                }}>
                                                <Icon name='keyboard-arrow-left' style={{
                                                        color: '#34495e',
                                                        fontSize: 16
                                                    }}/>
                                                <Text style={[
                                                        styles.fontSansLight, {
                                                            color: '#34495e',
                                                            fontSize: 11
                                                        }
                                                    ]}>نظر خود را ثبت کنید</Text>
                                            </Button>
                                        </Col>

                                        <Col style={{
                                                paddingTop: 10
                                            }}>
                                            <StarRating disabled={true} maxStars={5} rating={this.state.rating} starSize={20} starColor="#f1c40f"/>
                                        </Col>
                                    </Row>
                                </ListItem>
                            </List>
                        </CardItem>
                    </Card>

                    <Text style={[
                            styles.fontSansLight,
                            styles.wrapperHeader, {
                                fontSize: 13
                            }
                        ]}>دیگر محصول شرکت</Text>
                    <Card style={[{
                                width: width,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>

                            <ScrollView ref={(scrollView) => {
                                    _scrollView = scrollView;
                                }} automaticallyAdjustContentInsets={true} scrollEventThrottle={200} horizontal={true} style={[
                                    styles.scrollView, {
                                        paddingTop: 10,
                                        paddingBottom: 10
                                    }
                                ]}>
                                {
                                    this
                                        .state
                                        .pictures
                                        .map((s,
                                            i
                                        ) => {
                                            return <TouchableWithoutFeedback onPress={() => {
                                                    this.showGallery()
                                                }}>
                                                <View style={[{
                                                            backgroundColor: '#95a5a6',
                                                            height: 100,
                                                            width: 150,
                                                            marginLeft: 20,
                                                            justifyContent: 'center',
                                                            alignItems: 'center'
                                                        }
                                                    ]}>
                                                    <Image source={{
                                                            uri: s.url
                                                        }} style={[{
                                                                height: 100,
                                                                alignSelf: 'stretch',
                                                                textAlign: 'center'
                                                            }
                                                        ]}/>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        })
                                }
                            </ScrollView>

                        </CardItem>
                    </Card>

                    <Text style={[
                            styles.fontSansLight,
                            styles.wrapperHeader, {
                                fontSize: 13
                            }
                        ]}>محصولات مشابه</Text>
                    <Card style={[{
                                width: width,
                                alignSelf: 'center'
                            }
                        ]}>
                        <CardItem style={[{
                                    padding: 0
                                }
                            ]}>

                            <ScrollView ref={(scrollView) => {
                                    _scrollView = scrollView;
                                }} automaticallyAdjustContentInsets={true} scrollEventThrottle={200} horizontal={true} style={[
                                    styles.scrollView, {
                                        paddingTop: 10,
                                        paddingBottom: 10
                                    }
                                ]}>
                                {
                                    this
                                        .state
                                        .pictures
                                        .map((s,
                                            i
                                        ) => {
                                            return <TouchableWithoutFeedback onPress={() => {
                                                    this.showGallery()
                                                }}>
                                                <View style={[{
                                                            backgroundColor: '#95a5a6',
                                                            height: 100,
                                                            width: 150,
                                                            marginLeft: 20,
                                                            justifyContent: 'center',
                                                            alignItems: 'center'
                                                        }
                                                    ]}>
                                                    <Image source={{
                                                            uri: s.url
                                                        }} style={[{
                                                                height: 100,
                                                                alignSelf: 'stretch',
                                                                textAlign: 'center'
                                                            }
                                                        ]}/>
                                                </View>
                                            </TouchableWithoutFeedback>
                                        })
                                }
                            </ScrollView>

                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
