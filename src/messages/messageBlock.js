import React, { Component } from 'react';
import {
	Container,
	Content,
	Input,
	Label,
	Header,
	Title,
	Button,
	Form,
	Item,
	Left,
	Right,
	Col,
	Row,
	Badge
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import barghtheme from '../theme/barghchin';
import GiftedListView from 'react-native-gifted-listview';
import { Actions } from 'react-native-router-flux';
import {
	Image,
	Text,
	View,
	Dimensions,
	Animated,
	TouchableHighlight,
	TouchableWithoutFeedback,
	ScrollView,
	Keyboard
} from 'react-native';
import PopupMenu from '../common/PopupMenu';

import Api from '../helper/api';

var Styles = require('../style.js');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class MessageBlock extends Component {
	constructor(props) {
		super(props);

		this.state = { forceUpdate: false };
	}

	_onFetch = async (page = 1, callback, options) => {
		let response = await Api.request(
			'messages/contacts/block?page=' + page,
			'POST'
		);
		console.log(response);
		if (response[0] == 200) {
			return callback([response[1]['data']['contacts']]);
		}
	};

	_renderRowView(rowData) {
		return (
			<View>
				{rowData.length
					? <View style={[Styles.innerContainer]}>
							{rowData.map((s, i) => {
								return (
									<View>
											<View style={[Styles.contactItem]}>
												<Row>
                                                <Col>
                                                
										<Button
											onPress={() => {
												this.buyPlan(item.id);
											}}
											success
											style={{
												borderRadius: 4,
												elevation: 8,
                                                marginTop: 17,
                                                padding: 5
											}}>
											<Text
												style={[
													Styles.fontSans,
													{ color: '#fff' , fontSize: 11}
												]}>
												 حذف از لیست
											</Text>
										</Button>
                                        </Col>
													<Col
														style={{
															flexDirection: 'row',
															justifyContent:
																'flex-end'
														}}>
														<View
															style={[
																Styles.newIntro
															]}>
															<Text
																style={[
																	Styles.fontSans,
																	{
																		paddingBottom: 10
																	}
																]}>
																{s['title']}
															</Text>
															<Text
																style={[
																	Styles.fontSansLight,
																	{
																		fontSize: 11,
																		paddingBottom: 5
																	}
																]}>
																{s['contact'][
																	'first_name'
																] +
																	' ' +
																	s['contact'][
																		'last_name'
																	]}
															</Text>
															<Text
																style={[
																	Styles.fontSansLight,
																	{
																		fontSize: 11
																	}
																]}>
																تاریخ آخرین فعالیت
																:{' '}
																{s['last_update']['ago']}
															</Text>
														</View>
														<Image
															resizeMode={'cover'}
															source={{
																uri:
																	s['contact'][
																		'avatar'
																	]
															}}
															style={[
																Styles.messageListAvatar
															]}
														/>
													</Col>
												</Row>
											</View>
									</View>
								);
							})}
						</View>
					: <View />}
			</View>
		);
	}

	_renderPaginationWaitingView(paginateCallback) {
		return (
			<View style={{ marginVertical: 30 }}>
				<TouchableWithoutFeedback onPress={paginateCallback}>
					<View style={[Styles.loadMore]}>
						<Text
							style={[Styles.fontSansLight, Styles.loadMoreText]}>
							نمایش بیشتر
						</Text>
					</View>
				</TouchableWithoutFeedback>
			</View>
		);
	}

	_emptyDisplay() {
		return (
			<View style={[{ marginTop: 10, padding: 10, alignSelf: 'center' }]}>
				<Text style={[Styles.fontSans, { textAlign: 'center' }]}>
					نتیجه ای برای نمایش وجود ندارد .
				</Text>
			</View>
		);
	}

	render() {
		return (
			<Container>
					<Header
						style={[Styles.HeaderWrapper]}
						removeClippedSubviews={false}>
						<Left>
							<Button
								onPress={() => {
									Actions.MessageList({ type: 'reset' });
								}}
								transparent>
								<View style={[Styles.headerActionWrap]}>
									<Icon
										name="chevron-left"
										style={[Styles.headerActionIcon]}
									/>
									<Text
										style={[
											Styles.fontSans,
											Styles.headerActionText
										]}>
										بازگشت
									</Text>
								</View>
							</Button>
						</Left>
						<Right>
							<Title
								style={[
									Styles.fontSans,
									Styles.headerTitleRight,
									{
										fontSize: 14
									}
								]}>
								لیست مخاطبین بلاک شده
							</Title>
						</Right>
					</Header>

					<Content
						theme={barghtheme}
						style={[Styles.mainWrapper]}
						keyboardShouldPersistTaps="always">
						<GiftedListView
							enableEmptySections={true}
							ref="listview"
							emptyView={this._emptyDisplay}
							rowView={this._renderRowView.bind(this)}
							onFetch={this._onFetch}
							firstLoader={true}
							refreshable={false}
							pagination={true}
							forceUpdate={this.state.forceUpdate}
							withSections={false}
							customStyles={{
								paginationView: { backgroundColor: '#eee' }
							}}
							refreshableTintColor="blue"
							paginationWaitingView={
								this._renderPaginationWaitingView
							}
						/>
					</Content>
			</Container>
		);
	}
}
