import React, { Component } from 'react';
import {
	Container,
	Content,
	Input,
	Label,
	Header,
	Title,
	Button,
	Form,
	Item,
	Left,
	Right,
	Col,
	Row,
	Badge
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import barghtheme from '../theme/barghchin';
import {
	GiftedChat,
	LoadEarlier,
	MessageText,
	Bubble,
	Send
} from 'react-native-gifted-chat';
import { Actions } from 'react-native-router-flux';
import {
	Image,
	Text,
	View,
	Dimensions,
	Animated,
	TouchableHighlight,
	TouchableWithoutFeedback,
	ScrollView,
	Keyboard,
	TouchableOpacity
} from 'react-native';
import PopupMenu from '../common/PopupMenu';
import {
	Menu,
	MenuOptions,
	MenuOption,
	MenuTrigger,
	MenuContext
} from 'react-native-popup-menu';
var config = require('../config');
import Api from '../helper/api';

var Styles = require('../style.js');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const type = {
	company: 'شرکت',
	importer: 'وارد کننده',
	producer: 'تولید کننده',
	technician: 'تکنسین',	
};
var contact,
	contact_mine;

export default class MessageConversation extends Component {
	constructor(props) {
		super(props);

		this.state = { contactName: '', contactAvatar: config['server'] + '/default_avatar.png', contactEntity: 'company',
			messages: [] };
	}

	componentWillMount() {
		this._onFetch();
		this.setState({
			messages: [
				// {
				// 	_id: 1,
				// 	text: 'امیر خیرآبادی',
				// 	createdAt: new Date(),
				// 	user: {
				// 		_id: 2,
				// 		name: 'React Native',
				// 		avatar:
				// 			'https://facebook.github.io/react/img/logo_og.png'
				// 	}
				// },
				// {
				// 	_id: 2,
				// 	text: 'as خیرآبادی',
				// 	createdAt: new Date(),
				// 	user: {
				// 		_id: 1,
				// 		name: 'amir',
				// 		avatar:
				// 			'https://facebook.github.io/react/img/logo_og.png'
				// 	}
				// }
			]
		});
	}

	_onFetch = async () => {
		var _this = this;
		let response = await Api.request('messages', 'POST', {
			'entity': 'company',
			'id' : 1
		});
		if (response[0] == 200) {
			contact = response[1]['data']['contact'];
			contact_mine = response[1]['data']['contact_mine'];
			
			var messages = response[1]['data']['messages'];

			this.setState({
				contactName: contact.name,
				contactAvatar: contact.avatar,
			});			
			messages.forEach(function(message) {
				var new_message = { _id: message.id, text: message.text, createdAt: new Date(), user: { _id: message.owner != 'mine' ? 2 : 1, name: message.contact_id != contact.id ? contact_mine['name'] : this.state.contactName, avatar: message.owner != 'mine' ? _this.state.contactAvatar : contact_mine['avatar'] } };
				_this.setState(previousState => ({
					messages: GiftedChat.append(
						previousState.messages,
						new_message
					)
				}));
			});
		}
	};

	onSend = async (messages = []) => {
		let response = await Api.request('messages/send', 'POST', {
			'data': messages[0]['text'],
			'type' : 'text',
			'from': contact_mine['contact_id'],
			'to': contact['contact_id']
		});

		if (response[0] == 200) {
			this.setState(previousState => ({
				messages: GiftedChat.append(
					previousState.messages,
					messages
				)
			}));
		}
	}

	LoadEarlier(props) {
		return (
			<LoadEarlier
				{...props}
				label="بازگذاری پیام های قبلی"
				textStyle={[Styles.fontSans]}
			/>
		);
	}

	renderMessageText(props) {
		return (
			<MessageText
				{...props}
				textStyle={{
					left: [Styles.fontSans, { fontSize: 12 }],
					right: [Styles.fontSans, { fontSize: 12 }]
				}}
			/>
		);
	}

	renderBubble(props) {
		return (
			<Bubble
				{...props}
				wrapperStyle={{
					left: { backgroundColor: 'white' },
					right: { backgroundColor: '#2ecc71' }
				}}
			/>
		);
	}

	renderSend(props) {
        return (
			<TouchableOpacity style={{paddingHorizontal: 10, top: -10}} onPress={() => props.onSend({text: props.text}, true)}>
				<Icon name="paper-plane" style={{fontSize: 23}}/>
			</TouchableOpacity>
		);
    }

	render() {
		return <Container>
				<MenuContext>
					<Header style={[Styles.HeaderWrapper]} removeClippedSubviews={false}>
						<Left>
							<Button onPress={() => {
									Actions.MessageList({
										type: 'reset'
									});
								}} transparent>
								<View style={[Styles.headerActionWrap]}>
									<Icon name="chevron-left" style={[Styles.headerActionIcon]} />
									<Text
										style={[
											Styles.fontSans,
											Styles.headerActionText
										]}>
										بازگشت
									</Text>
								</View>
							</Button>
						</Left>
						<Right>
							<View>
								<Title style={[Styles.fontSans, Styles.headerTitleRight, { fontSize: 14, top: -15 }]}>
									{(this.state.contactEntity && this.state.contactEntity != "user") && <Text style={[Styles.fontSans, { paddingRight: 10 }]}>
											({type[this.state.contactEntity]})
										</Text>}
									{'   ' + this.state.contactName}
								</Title>
							</View>

							<Image source={{ uri: this.state.contactAvatar }} style={[Styles.messageAvatar]} />
						</Right>
					</Header>

					<Content theme={barghtheme} style={[Styles.mainWrapper]} keyboardShouldPersistTaps="always">
						<View style={{ width: width, height: height - 80 }}>
							<GiftedChat showUserAvatar={true} locale="fa" placeholder="پیام خود را تایپ کنید ." isAnimated={true} loadEarlier={false} renderLoadEarlier={this.LoadEarlier} renderBubble={this.renderBubble} renderMessageText={this.renderMessageText} messages={this.state.messages} onSend={messages => this.onSend(messages)} user={{ _id: 1 }} renderSend={this.renderSend}/>
						</View>
					</Content>
				</MenuContext>
			</Container>;
	}
}
