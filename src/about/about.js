import React, { Component } from "react";
import {
  Container,
  Grid,
  Row,
  Content,
  Header,
  Title,
  Button,
  Fab,
  Left,
  Right
} from "native-base";
import barghtheme from "../theme/barghchin";
import { Actions } from "react-native-router-flux";
import { Text, Dimensions, WebView, View, BackAndroid } from "react-native";
import Share from "react-native-share";
import Icon from "react-native-vector-icons/FontAwesome";
var config = require("../config.js");
var Styles = require("../style.js");
export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }
  componentDidMount() {
    BackAndroid.addEventListener("hardwareBackPress", () => {
      Actions.pop();
      return true;
    });
  }

  onCancel() {
    this.setState({ visible: false });
  }
  onOpen() {
    this.setState({ visible: true });
  }

  onNavigationStateChange(event) {
    if (event.title) {
      const htmlHeight = Number(event.title); //convert to number
      this.setState({ Height: htmlHeight });
    }
  }

  render() {
    let shareOptions = {
      title: "معرفی به دوستان",
      message: "برق چین  : barghchin.com",
      subject: "معرفی به دوستان" //  for email
    };

    const width = Dimensions.get("window").width;
    const height = Dimensions.get("window").height;
    return <Container>
        <Header style={[Styles.HeaderWrapper]}>
          <Left>
            <Button onPress={() => {
                Actions.Home({ type: "reset" });
              }} transparent>
              <View style={[Styles.headerActionWrap]}>
                <Icon name="chevron-left" style={[Styles.headerActionIcon]} />
                <Text style={[Styles.fontSans, Styles.headerActionText]}>
                  بازگشت
                </Text>
              </View>
            </Button>
          </Left>
          <Right>
            <Title style={[Styles.fontSans, Styles.headerTitleRight]}>
              درباره ما
            </Title>
          </Right>
        </Header>
        <Content theme={barghtheme}>
          <View style={[Styles.innerContainer]}>
            <WebView scrollEnabled={false} javaScriptEnabled={true} scrollEnabled={false} onNavigationStateChange={this.onNavigationStateChange.bind(this)} style={[{ flex: 1, height: this.state.Height }]} source={{ uri: config.server + "/api/page/aboutus" }} />
          </View>
        </Content>
        <Fab direction="right" containerStyle={{ marginLeft: 10 }} style={{ backgroundColor: "#5c4b8c" }} position="bottomLeft" onPress={() => Share.open(shareOptions)}>
          <Icon name="share-alt" />
        </Fab>
      </Container>;
  }
}
