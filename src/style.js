import {StyleSheet, Dimensions} from 'react-native';
const width = Dimensions
    .get('window')
    .width;
const height = Dimensions
    .get('window')
    .height;
module.exports = StyleSheet.create({
	// Global
	fontSans: {
		fontFamily: 'IRANSans_Medium'
	},
	fontSansLight: {
		fontFamily: 'IRANSans_Light'
	},

	mainWrapper: {
		backgroundColor: '#ecf0f1'
	},
	HeaderWrapper: {
		flexDirection: 'row',
		elevation: 5,
		backgroundColor: '#5c4b8c',
		height: 60
	},
	moduleHeader: {
		fontSize: 14,
		color: '#2c3e50',
		paddingTop: 15,
		paddingRight: 10
	},
	container: {
		width: width,
		marginTop: 10,
		alignSelf: 'center'
	},
	innerContainerClear: {
		width: width - 20,
		alignSelf: 'center',
		marginVertical: 10,
		padding: 10
	},
	innerContainer: {
		width: width - 40,
		alignSelf: 'center',
		backgroundColor: '#fff',
		elevation: 5,
		marginVertical: 20,
		padding: 10,
		borderRadius: 4
	},
	simpleBlock: {
		backgroundColor: '#fff',
		elevation: 5,
		marginVertical: 10,
		borderRadius: 4
	},

	blockHeader: {
		paddingRight: 20,
		paddingVertical: 15
	},
	blockHeaderText: {
		color: '#6d8093'
	},

	// Header
	headerSection: {
		height: 60,
		backgroundColor: '#5c4b8c',
		paddingTop: 5,
		paddingBottom: 5
	},

	headerTitleRight: {
		color: '#fff',
		textAlign: 'center',
		alignSelf: 'flex-end',
		fontSize: 16,
		paddingRight: 10
	},
	headerActionWrap: {
		flexDirection: 'row'
	},
	headerActionIcon: {
		fontSize: 14,
		color: '#fff',
		marginTop: 4
	},
	headerActionText: {
		fontSize: 15,
		color: '#fff',
		paddingLeft: 10
	},
	// Menu
	menuWapper: {
		flex: 1,
		backgroundColor: '#fff'
	},
	menuList: {},
	menuItems: {
		padding: 10,
		paddingTop: 5,
		justifyContent: 'flex-end',
		borderWidth: 0
	},
	menuUser: {
		backgroundColor: '#5c4b8c'
	},

	// Home
	homeBody: {
		flex: 1
	},

	//Auth
	authWapper: {
		flex: 1,
		backgroundColor: '#5c4b8c'
	},
	authInputs: {
		backgroundColor: 'rgba(255,255,255,.3)',
		borderWidth: 0
	},
	authInputText: {
		color: '#fff',
		marginRight: 10,
		fontSize: 15,
		marginTop: 5,
		textAlign: 'right'
	},
	authButton: {
		borderWidth: 1,
		borderColor: '#fff',
		marginTop: 20
	},
	authButtonText: {
		color: '#fff',
		alignItems: 'flex-start'
	},
	authInputIcon: {
		fontSize: 20,
		color: '#fff',
		paddingRight: 15
	},
	authModalWrapper: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(92, 75, 140,.8)'
	},
	authModal: {
		padding: 10,
		backgroundColor: '#fff',
		alignItems: 'flex-end',
		width: 300,
		height: 250
	},

	otherModal: {
		padding: 10,
		backgroundColor: '#fff',
		alignItems: 'flex-end',
		width: 300
	},

	// News
	newsItem: {
		flexDirection: 'row',
		alignItems: 'flex-start',
		alignSelf: 'flex-end',
		padding: 10
	},
	newIntro: {
		paddingRight: 20
	},
	newsCover: {
		height: 80,
		width: 80,
		alignSelf: 'flex-start',
		borderRadius: 4
	},
	loadMore: {
		alignSelf: 'center',
		marginTop: 5,
		backgroundColor: '#5c4b8c',
		paddingVertical: 5,
		paddingHorizontal: 20,
		elevation: 5,
		marginBottom: 10
	},
	loadMoreText: {
		fontSize: 13,
		color: '#fff'
	},

	// Loading
	loadingWrapper: {
		width: width,
		height: height,
		zIndex: 1,
		backgroundColor: 'rgba(92, 75, 140,.9)',
		alignItems: 'center',
		justifyContent: 'center'
	},
	loadingLayout: {
		alignSelf: 'center',
		padding: 5,
		paddingVertical: 10,
		borderRadius: 4
	},
	loadingText: {
		fontSize: 16,
		paddingTop: 30,
		alignSelf: 'center',
		color: '#fff'
	},
	loadingSpinner: {
		top: 0,
		alignItems: 'center'
	},

	// modules
	module_image_sample: {
		height: 100,
		width: width / 2 - 30,
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		alignSelf: 'stretch'
	},

	// search
	searchButton: {
		backgroundColor: '#5c4b8c'
	},
	searchIcon: {
		color: '#fff'
	},

	// Profile
	profileAvatarWrapper: {
		top: 90,
		left: 40,
		width: 100,
		height: 100,
		position: 'absolute',
		borderRadius: 100,
		shadowColor: '#000000',
		shadowOpacity: 0.8,
		shadowRadius: 2,
		shadowOffset: {
			height: 1,
			width: 0
		},
		elevation: 8,
		overflow: 'visible'
	},
	profileAvatar: {
		width: 100,
		height: 100,
		borderRadius: 100,
		overflow: 'visible'
	},
	profileBussinessItem: {
		borderRadius: 4,
		backgroundColor: '#fff',
		elevation: 10,
		width: (width - 70) / 4,
		marginHorizontal: 5,
		padding: 9
	},
	profileBussinessImage: {
		width: 40,
		height: 40,
		alignSelf: 'center'
	},
	profileBussinessText: {
		fontSize: 12,
		marginTop: 10,
		alignSelf: 'center'
	},

	// Alerts
	alertInfo: {
		backgroundColor: '#d9edf7',
		width: width - 40,
		alignSelf: 'center',
		padding: 10,
		paddingVertical: 5,
		marginVertical: 10,
		borderRadius: 4
	},
	alertInfoText: {
		color: '#31708f',
		fontSize: 13
	},

	// Messages
	messageListAvatar: {
		height: 80,
		width: 80,
		borderRadius: 100
	},
	contactItem: {
		padding: 0
	},
	messageAvatar :{
		width: 50,
		height: 50,
		borderRadius: 50
	}
});
