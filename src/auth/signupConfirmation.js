import React, { Component } from "react";
import {
  Container,
  Content,
  InputGroup,
  Input,
  Header,
  Title,
  Button,
  Row,
  Right,
  Left
} from "native-base";
import barghtheme from "../theme/barghchin";
import Icon from "react-native-vector-icons/FontAwesome";
import { Actions } from "react-native-router-flux";
import {
  Image,
  Text,
  View,
  TouchableWithoutFeedback,
  Dimensions,
  BackAndroid,
  Keyboard
} from "react-native";
import SmsListener from "react-native-android-sms-listener";
import Toast from "react-native-root-toast";
import { Bubbles, DoubleBounce, Bars, Pulse } from "react-native-loader";
import Api from "../helper/api";

var Styles = require("../style.js");
export default class SignupConfirmation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobile: this.props.data.mobile,
      email: this.props.data.email,
      type: this.props.data.type,
      confirmation: ""
    };
  }

  componentDidMount() {
    BackAndroid.addEventListener("hardwareBackPress", () => {
      Actions.Home();
      return true;
    });

    if (this.state.mobile) {
      let subscription = SmsListener.addListener(message => {
        let verificationCodeRegex = /کد فعال سازی شما برای ثبت نام در برق چین : [\d]{5}/;

        if (verificationCodeRegex.test(message.body)) {
          let verificationCode = message.body.match(/[\d]{5}/)[0];
          this.setState({
            confirmation: verificationCode
          });
          subscription.remove();
          this.check();
        }
      });
    }

    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      () => {
        this.setState({ keyBoardShow: true });
      }
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      () => {
        this.setState({ keyBoardShow: false });
      }
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  async check() {
    if (!this.state.confirmation) {
      Toast.show("لطفا همه فیلد ها را تکمیل نمایید .", {
        duration: Toast.durations.LONG,
        position: -50,
        backgroundColor: "#e74c3c",
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        textStyle: {
          color: "#fff",
          fontFamily: "IRANSans_Light"
        }
      });
      return;
    }
    Keyboard.dismiss();
    this.setState({ onWorking: true });
    let response = await Api.request("auth/check", "POST", {
      mobile: this.state.mobile,
      email: this.state.email,
      type: this.state.type,
      confirmation: this.state.confirmation
    });

    if (response[0] == 200) {
      Actions.SignupComplete({
        data: {
          mobile: this.state.mobile,
          confirmation: this.state.confirmation,
          email: this.state.email,
          type: this.state.type
        }
      });
    }
    this.setState({ onWorking: false });
  }

  render() {
    const width = Dimensions.get("window").width;
    const height = Dimensions.get("window").height;
    return (
      <Container style={Styles.authWapper}>
        <Image
          source={require("../resource/images/bg_main.png")}
          style={[
            {
              flex: 1,
              width: null,
              height: null
            }
          ]}
        >
          <Content theme={barghtheme} keyboardShouldPersistTaps="always">
            <Header
              style={[{ backgroundColor: "rgba(255,255,255,0.1)", height: 60 }]}
            >
              <Left>
                <Button
                  onPress={() => {
                    Actions.Signup();
                  }}
                  transparent
                >
                  <View style={[Styles.headerActionWrap]}>
                    <Icon
                      name="chevron-left"
                      style={[Styles.headerActionIcon]}
                    />
                    <Text
                      style={[
                        Styles.fontSans,
                        Styles.headerActionText,
                      ]}
                    >
                      بازگشت
                    </Text>
                  </View>
                </Button>
              </Left>
              <Right />
            </Header>

            <View
              style={[
                {
                  height: height - 140
                }
              ]}
            >
              {!this.state.keyBoardShow
                ? <View
                    style={[
                      {
                        alignSelf: "center",
                        marginTop: height * 0.1
                      }
                    ]}
                  >
                    <Image
                      source={require("../resource/images/logo_main.png")}
                      style={[
                        {
                          width: 120,
                          height: 136
                        }
                      ]}
                    />
                  </View>
                : <View />}
              <View
                style={[
                  {
                    alignSelf: "center",
                    width: width * 0.8,
                    marginTop: height * 0.1
                  }
                ]}
              >
                <Text
                  style={[
                    Styles.fontSans,
                    {
                      color: "#fff",
                      alignSelf: "center",
                      textAlign: "center",
                      fontSize: 12
                    }
                  ]}
                >
                  لطفا کد فعال سازی که برای شما ارسال شده است را در باکس زیر
                  وارد کنید .
                </Text>
              </View>
              <View
                style={[
                  {
                    alignSelf: "center",
                    width: width * 0.8,
                    marginTop: height * 0.05
                  }
                ]}
              >
                <InputGroup
                  iconRight
                  style={[
                    Styles.authInputs,
                    {
                      alignSelf: "center"
                    }
                  ]}
                >
                  <Input
                    style={[Styles.fontSans, Styles.authInputText]}
                    keyboardType="numeric"
                    value={this.state.confirmation}
                    placeholderTextColor="white"
                    placeholder="کد فعال سازی"
                    onChangeText={text => {
                      this.setState({
                        confirmation: text
                      });
                    }}
                  />
                  <Icon
                    name="key"
                    style={[Styles.authInputIcon, { fontSize: 23 }]}
                  />
                </InputGroup>
              </View>

              {this.state.onWorking
                ? <View
                    style={[
                      {
                        alignSelf: "center",
                        alignItems: "center",
                        marginTop: 30,
                        flexDirection: "row"
                      }
                    ]}
                  >
                    <Bubbles size={13} color="#FF4C74" />
                  </View>
                : <Button
                    onPress={() => {
                      this.check();
                    }}
                    transparent
                    textStyle={[Styles.fontSans, Styles.authButtonText]}
                    style={[
                      Styles.authButton,
                      {
                        alignSelf: "center",
                        marginTop: 30,
                        width: 250
                      }
                    ]}
                  >
                    <Text
                      style={[
                        Styles.fontSans,
                        {
                          color: "#fff",
                          width: 150
                        }
                      ]}
                    >
                      بررسی کد فعال سازی
                    </Text>
                  </Button>}
            </View>
          </Content>
        </Image>
      </Container>
    );
  }
}
