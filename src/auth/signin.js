import React, { Component } from 'react';
import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Button,
	Row,
	Footer,
	Left,
	Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import {
	Image,
	Text,
	View,
	TouchableWithoutFeedback,
	Dimensions,
	BackAndroid,
	Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
import Api from '../helper/api';
import SmsListener from 'react-native-android-sms-listener';

var Styles = require('../style.js');
export default class Signin extends Component {
	constructor(props) {
		super(props);

		this.state = {
			input: '',
			password: '',
			modalVisible: false,

			onWorking: false,
			keyBoardShow: false
		};
	}

	componentDidMount() {
		BackAndroid.addEventListener('hardwareBackPress', () => {
			Actions.Home();
			return true;
		});

		this.keyboardDidShowListener = Keyboard.addListener(
			'keyboardDidShow',
			() => {
				this.setState({
					keyBoardShow: true
				});
			}
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			'keyboardDidHide',
			() => {
				this.setState({
					keyBoardShow: false
				});
			}
		);
	}

	componentWillMount() {
		BackAndroid.addEventListener('hardwareBackPress', () => {
			Actions.Home();
			return true;
		});
	}

	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	async signin() {
		if (!this.state.input || !this.state.password) {
			Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#e74c3c',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontFamily: 'IRANSans_Light',
					fontSize: 15
				}
			});
			return;
		}
		this.setState({
			onWorking: true
		});
		Keyboard.dismiss();
		let response = await Api.request('auth/signin', 'POST', {
			input: this.state.input,
			password: this.state.password
		});
		if (response[0] == 200) {
			let store = await Api.userStore(
				JSON.stringify(response[1]['data'])
			);
			Actions.Home();
		}
		this.setState({
			onWorking: false
		});
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;
		return (
			<Container style={Styles.authWapper}>
				<Image
					source={require('../resource/images/bg_main.png')}
					style={[{ flex: 1, width: null, height: null }]}>
					<Content
						theme={barghtheme}
						keyboardShouldPersistTaps="always">
						<Header
							style={[
								{
									backgroundColor: 'rgba(255,255,255,0.1)',
									height: 60
								}
							]}>
							<Left>
								<Button
									onPress={() => {
										Actions.Home({ type: 'reset' });
									}}
									transparent>
									<View style={[Styles.headerActionWrap]}>
										<Icon
											name="chevron-left"
											style={[Styles.headerActionIcon]}
										/>
										<Text
											style={[
												Styles.fontSans,
												Styles.headerActionText
											]}>
											بازگشت
										</Text>
									</View>
								</Button>
							</Left>
							<Right />
						</Header>
						<View style={[{ height: height - 140 }]}>
							{!this.state.keyBoardShow
								? <View
										style={[
											{
												alignSelf: 'center',
												marginTop: height * 0.1
											}
										]}>
										<Image
											source={require('../resource/images/logo_main.png')}
											style={[{ width: 120, height: 136 }]}
										/>
									</View>
								: <View />}
							<View
								style={[
									{
										alignSelf: 'center',
										width: width * 0.8,
										marginTop: height * 0.08
									}
								]}>
								<InputGroup
									iconRight
									style={[
										Styles.authInputs,
										{ alignSelf: 'center' }
									]}>
									<Input
										style={[
											Styles.fontSans,
											Styles.authInputText
										]}
										placeholderTextColor="white"
										placeholder="شماره موبایل و یا پست الکترونیک"
										onChangeText={text => {
											this.setState({ input: text });
										}}
									/>
									<Icon
										name="mobile"
										style={[
											Styles.authInputIcon,
											{ fontSize: 26 }
										]}
									/>
								</InputGroup>
							</View>
							<View
								style={[
									{
										alignSelf: 'center',
										width: width * 0.8,
										marginTop: height * 0.03
									}
								]}>
								<InputGroup
									iconRight
									style={[
										Styles.authInputs,
										{ alignSelf: 'center' }
									]}>
									<Input
										style={[
											Styles.fontSans,
											Styles.authInputText
										]}
										secureTextEntry={true}
										placeholderTextColor="white"
										placeholder="رمز عبور"
										onChangeText={text => {
											this.setState({ password: text });
										}}
									/>
									<Icon
										name="key"
										style={[Styles.authInputIcon]}
									/>
								</InputGroup>
							</View>
							{this.state.onWorking
								? <View
										style={[
											{
												alignSelf: 'center',
												alignItems: 'center',
												marginTop: 30,
												flexDirection: 'row'
											}
										]}>
										<Bubbles size={13} color="#FF4C74" />
									</View>
								: <View style={{ alignItems: 'center' }}>
										<Button
											onPress={() => {
												this.signin();
											}}
											transparent
											style={[
												Styles.authButton,
												{
													alignSelf: 'center',
													marginTop: 30,
													flexDirection: 'row'
												}
											]}>
											<Text
												style={[
													Styles.fontSans,
													Styles.authButtonText,
													{ color: '#fff' }
												]}>
												ورود به حساب کاربری
											</Text>
										</Button>
									</View>}
							<Button
								onPress={() => {
									Actions.Forget();
								}}
								transparent
								style={[
									{ alignSelf: 'center', marginTop: 15 }
								]}>
								<Text
									style={[
										Styles.fontSans,
										{ color: '#fff' }
									]}>
									رمز عبور خود را فراموش کرده اید ؟
								</Text>
							</Button>
						</View>
						<View
							style={[
								{
									backgroundColor: 'rgba(255,255,255,.1)',
									paddingTop: 20,
									height: 60
								}
							]}>
							<TouchableWithoutFeedback
								onPress={() => {
									Actions.Signup();
								}}
								underlayColor="#5c4b8c">
								<View>
									<Text
										style={[
											Styles.fontSans,
											{
												color: '#fff',
												textAlign: 'center'
											}
										]}>
										هنوز در برق چین ثبت نام نکرده اید ؟
									</Text>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</Content>
				</Image>
			</Container>
		);
	}
}
