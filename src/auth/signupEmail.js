import React, { Component } from "react";
import {
    Container,
    Content,
    InputGroup,
    Input,
    Header,
    Title,
    Button,
    Row,
    Left,
    Right
} from "native-base";
import barghtheme from "../theme/barghchin";
import { Actions } from "react-native-router-flux";
import {
    Image,
    Text,
    Modal,
    View,
    TouchableWithoutFeedback,
    Dimensions,
    BackAndroid,
    Keyboard
} from "react-native";
import Toast from "react-native-root-toast";
import Icon from "react-native-vector-icons/FontAwesome";
import { Bubbles, DoubleBounce, Bars, Pulse } from "react-native-loader";
import Api from "../helper/api";

var Styles = require("../style.js");
export default class SignupEmail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            last_name: "",
            first_name: "",
            type: "email",
            modalVisible: false,

            onWorking: false,
            keyBoardShow: false
        };
    }

    componentDidMount() {
        BackAndroid.addEventListener("hardwareBackPress", () => {
            Actions.pop();
            return true;
        });

        this.keyboardDidShowListener = Keyboard.addListener(
            "keyboardDidShow",
            () => {
                this.setState({ keyBoardShow: true });
            }
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            "keyboardDidHide",
            () => {
                this.setState({ keyBoardShow: false });
            }
        );
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    showModal(visible) {
        if (
            !this.state.email ||
            !this.state.last_name ||
            !this.state.first_name
        ) {
            Toast.show("لطفا همه فیلد ها را تکمیل نمایید .", {
                duration: Toast.durations.LONG,
                position: -50,
                backgroundColor: "#e74c3c",
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
                textStyle: {
                    color: "#fff",
                    fontFamily: "IRANSans_Light"
                }
            });
            return;
        }
        Keyboard.dismiss();
        this.setState({ modalVisible: visible });
    }

    async register() {
        this.setState({ modalVisible: false, onWorking: true });
        let response = await Api.request("auth/signup", "POST", {
            email: this.state.email,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            type: "email"
        });
        if (response[0] == 200) {
            Actions.SignupConfirmation({
                data: {
                    email: this.state.email,
                    type: this.state.type
                }
            });
        }
        this.setState({ onWorking: false });
    }

    render() {
        const width = Dimensions.get("window").width;
        const height = Dimensions.get("window").height;
        return (
            <Container style={Styles.authWapper}>
                <Image
                    source={require("../resource/images/bg_main.png")}
                    style={[
                        {
                            flex: 1,
                            width: null,
                            height: null
                        }
                    ]}
                >
                    <Content
                        theme={barghtheme}
                        keyboardShouldPersistTaps="always"
                    >
                        <Modal
                            styles={{
                                flex: 1,
                                backgroundColor: "#000"
                            }}
                            animationType={"fade"}
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.showModal(!this.state.modalVisible);
                            }}
                        >
                            <Container style={Styles.authModalWrapper}>
                                <View style={Styles.authModal}>
                                    <View>
                                        <Text
                                            style={[
                                                Styles.fontSans,
                                                {
                                                    fontSize: 15
                                                }
                                            ]}
                                        >
                                            تایید آدرس ایمیل
                                        </Text>
                                    </View>
                                    <Row>
                                        <Text
                                            style={[
                                                Styles.fontSans,
                                                {
                                                    fontSize: 13
                                                }
                                            ]}
                                        >
                                            در صورت تایید شما ایمیلی حاوی کد
                                            فعال سازی حساب کاربری به ایمیل شما
                                            ارسال خواهد شد .
                                        </Text>
                                    </Row>
                                    <Row
                                        style={{
                                            alignSelf: "center",
                                            marginTop: 0
                                        }}
                                    >
                                        <Text
                                            style={[
                                                Styles.fontSans,
                                                {
                                                    fontSize: 13,
                                                    color: "#5c4b8c"
                                                }
                                            ]}
                                        >
                                            {this.state.email}
                                        </Text>
                                    </Row>
                                    <Row>
                                        <Button
                                            onPress={() => {
                                                this.showModal(false);
                                            }}
                                            transparent
                                            bordered
                                            iconRight="iconRight"
                                            style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120
                                            }}
                                            textStyle={{
                                                marginRight: -30,
                                                fontSize: 11
                                            }}
                                        >
                                            <Text style={[Styles.fontSans]}>
                                                انصراف
                                            </Text>
                                            <Icon name="close" />
                                        </Button>
                                        <Button
                                            onPress={() => {
                                                this.register();
                                            }}
                                            transparent
                                            bordered
                                            iconRight="iconRight"
                                            style={{
                                                padding: 0,
                                                margin: 0,
                                                width: 120,
                                                marginLeft: 40
                                            }}
                                            textStyle={{
                                                marginRight: -30,
                                                fontSize: 11
                                            }}
                                        >
                                            <Text style={[Styles.fontSans]}>
                                                تایید
                                            </Text>
                                            <Icon name="check" />
                                        </Button>
                                    </Row>
                                </View>
                            </Container>
                        </Modal>

                        <Header
                            style={[
                                {
                                    backgroundColor: "rgba(255,255,255,0.1)",
                                    height: 60
                                }
                            ]}
                        >
                            <Left>
                                <Button
                                    onPress={() => {
                                        Actions.Signup();
                                    }}
                                    transparent
                                >
                                    <View style={[Styles.headerActionWrap]}>
                                        <Icon
                                            name="chevron-left"
                                            style={[Styles.headerActionIcon]}
                                        />
                                        <Text
                                            style={[
                                                Styles.fontSans,
                                                Styles.headerActionText,
                                                {
                                                    fontSize: 14,
                                                    paddingRight: 10
                                                }
                                            ]}
                                        >
                                            ثبت نام از طریق پیامک
                                        </Text>
                                    </View>
                                </Button>
                            </Left>
                            <Right />
                        </Header>
                        <View
                            style={[
                                {
                                    height: height - 140
                                }
                            ]}
                        >
                            {!this.state.keyBoardShow
                                ? <View
                                      style={[
                                          {
                                              alignSelf: "center",
                                              marginTop: height * 0.05
                                          }
                                      ]}
                                  >
                                      <Image
                                          source={require("../resource/images/logo_main.png")}
                                          style={[
                                              {
                                                  width: 120,
                                                  height: 136
                                              }
                                          ]}
                                      />
                                  </View>
                                : <View />}

                            <View
                                style={[
                                    {
                                        alignSelf: "center",
                                        width: width * 0.8,
                                        marginTop: height * 0.05
                                    }
                                ]}
                            >
                                <InputGroup
                                    iconRight
                                    style={[
                                        Styles.authInputs,
                                        {
                                            alignSelf: "center"
                                        }
                                    ]}
                                >
                                    <Input
                                        style={[
                                            Styles.fontSans,
                                            Styles.authInputText
                                        ]}
                                        placeholderTextColor="white"
                                        placeholder="پست الکترونیک"
                                        value={this.state.email}
                                        onChangeText={text => {
                                            this.setState({ email: text });
                                        }}
                                    />
                                    <Icon
                                        name="envelope-o"
                                        style={[
                                            Styles.authInputIcon,
                                            { fontSize: 23 }
                                        ]}
                                    />
                                </InputGroup>
                            </View>

                            <View
                                style={[
                                    {
                                        alignSelf: "center",
                                        width: width * 0.8,
                                        marginTop: 20
                                    }
                                ]}
                            >
                                <InputGroup
                                    iconRight
                                    style={[
                                        Styles.authInputs,
                                        {
                                            alignSelf: "center"
                                        }
                                    ]}
                                >
                                    <Input
                                        style={[
                                            Styles.fontSans,
                                            Styles.authInputText
                                        ]}
                                        placeholderTextColor="white"
                                        placeholder="نام"
                                        value={this.state.first_name}
                                        onChangeText={text => {
                                            this.setState({ first_name: text });
                                        }}
                                    />
                                    <Icon
                                        name="user"
                                        style={[
                                            Styles.authInputIcon,
                                            { fontSize: 23 }
                                        ]}
                                    />
                                </InputGroup>
                            </View>
                            <View
                                style={[
                                    {
                                        alignSelf: "center",
                                        width: width * 0.8,
                                        marginTop: 10
                                    }
                                ]}
                            >
                                <InputGroup
                                    iconRight
                                    style={[
                                        Styles.authInputs,
                                        {
                                            alignSelf: "center"
                                        }
                                    ]}
                                >
                                    <Input
                                        style={[
                                            Styles.fontSans,
                                            Styles.authInputText
                                        ]}
                                        placeholderTextColor="white"
                                        placeholder="نام خانوادگی"
                                        value={this.state.last_name}
                                        onChangeText={text => {
                                            this.setState({ last_name: text });
                                        }}
                                    />
                                    <Icon
                                        name="user"
                                        style={[
                                            Styles.authInputIcon,
                                            { fontSize: 23 }
                                        ]}
                                    />
                                </InputGroup>
                            </View>
                            <Button
                                onPress={() => {
                                    Actions.Rules();
                                }}
                                transparent
                                style={[
                                    {
                                        marginTop: 20,
                                        alignSelf: "center"
                                    }
                                ]}
                            >
                                <Text
                                    underlineColorAndroid={"blue"}
                                    style={[
                                        Styles.fontSans,
                                        {
                                            color: "#fff",
                                            textAlign: "center",
                                            textDecorationLine: "underline"
                                        }
                                    ]}
                                >
                                    قوانین برق چین
                                </Text>
                            </Button>
                            {this.state.onWorking
                                ? <View
                                      style={[
                                          {
                                              alignSelf: "center",
                                              alignItems: "center",
                                              marginTop: 30,
                                              flexDirection: "row"
                                          }
                                      ]}
                                  >
                                      <Bubbles size={13} color="#FF4C74" />
                                  </View>
                                : <Button
                                      onPress={() => {
                                          this.showModal(
                                              !this.state.modalVisible
                                          );
                                      }}
                                      transparent
                                      textStyle={[
                                          Styles.fontSans,
                                          Styles.authButtonText
                                      ]}
                                      style={[
                                          Styles.authButton,
                                          {
                                              alignSelf: "center",
                                              marginTop: 20,
                                              width: 200
                                          }
                                      ]}
                                  >
                                      <Text
                                          style={[
                                              Styles.fontSans,
                                              {
                                                  color: "#fff",
                                                  width: 100
                                              }
                                          ]}
                                      >
                                          ثبت نام
                                      </Text>
                                  </Button>}
                        </View>
                        <View
                            style={[
                                {
                                    backgroundColor: "rgba(255,255,255,.1)",
                                    paddingTop: 20,
                                    height: 60
                                }
                            ]}
                        >
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    Actions.Signin();
                                }}
                                underlayColor="#5c4b8c"
                            >
                                <View>
                                    <Text
                                        style={[
                                            Styles.fontSans,
                                            {
                                                color: "#fff",
                                                textAlign: "center"
                                            }
                                        ]}
                                    >
                                        قبلا در سیستم ثبت نام کرده اید ؟
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </Content>
                </Image>
            </Container>
        );
    }
}
