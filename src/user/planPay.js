import React, { Component } from 'react';
import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Button,
	Row,
	Col,
	Picker,
	Item,
	Left,
	Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Icon from 'react-native-vector-icons/FontAwesome';
import Toast from 'react-native-root-toast';
import { Actions } from 'react-native-router-flux';
import {
	Text,
	View,
	Dimensions,
	TouchableWithoutFeedback,
	Modal,
	BackAndroid,
	Keyboard,
	Image,
	WebView
} from 'react-native';
import Api from '../helper/api';
var Styles = require('../style.js');
export default class PlanPay extends Component {
	constructor(props) {
		super(props);
		this.state = {
			url: this.props.url
		};
	}

	navigationChange(data) {
		console.log(data, data.url.search('/plan/result?id=success'), data.loading);
		if (data.url.indexOf('/plan/result?id=success') != -1 && !data.loading) {
			Actions.Plan({ type: 'reset', pay: 'success' });
		}

		if (data.url.indexOf('/plan/result?id=failed') != -1 && !data.loading) {
			Actions.Plan({ type: 'reset', pay: 'failed' });
		}

		if (data.url.indexOf('/plan/result?id=cancel') != -1 && !data.loading) {
			Actions.Plan({ type: 'reset', pay: 'cancel' });
		}
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;

		return (
			<Container>
				<Content
					theme={barghtheme}
					style={[Styles.mainWrapper]}
					keyboardShouldPersistTaps="always">
					<View>
						<WebView
							scrollEnabled={false}
							onNavigationStateChange={this.navigationChange}
							javaScriptEnabled={true}
							scrollEnabled={false}
							style={[{ flex: 1, height: height, width: width }]}
							source={{ uri: this.state.url }}
						/>
					</View>
				</Content>
			</Container>
		);
	}
}
