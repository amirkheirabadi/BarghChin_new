import React, { Component } from 'react';
import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Button,
	Row,
	Footer,
	Col,
	Grid,
	Left,
	Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import { Actions } from 'react-native-router-flux';
import {
	Image,
	Text,
	Modal,
	View,
	TouchableWithoutFeedback,
	Dimensions,
	BackAndroid,
	Keyboard
} from 'react-native';
import Toast from 'react-native-root-toast';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
import Api from '../helper/api';

var Styles = require('../style.js');
import SmsListener from 'react-native-android-sms-listener';
export default class ProfileConfirm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			type: 'mobile', //this.props.data.type,
			confirmation: '',

			onWorking: false,
			keyBoardShow: false
		};
	}

	componentDidMount() {
		BackAndroid.addEventListener('hardwareBackPress', () => {
			Actions.Settings();
			return true;
		});

		let subscription = SmsListener.addListener(message => {
			let verificationCodeRegex = /کد تایید برای تغییر شماره تلفن همراه : [\d]{5}/;

			if (verificationCodeRegex.test(message.body)) {
				let verificationCode = message.body.match(/[\d]{5}/)[0];
				this.setState({ confirmation: verificationCode });
				subscription.remove();
				this.confirm();
			}
		});
		this.keyboardDidShowListener = Keyboard.addListener(
			'keyboardDidShow',
			() => {
				this.setState({ keyBoardShow: true });
			}
		);
		this.keyboardDidHideListener = Keyboard.addListener(
			'keyboardDidHide',
			() => {
				this.setState({ keyBoardShow: false });
			}
		);
	}

	async confirm() {
		if (!this.state.confirmation) {
			Toast.show('لطفا همه فیلد ها را تکمیل نمایید .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#e74c3c',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontFamily: 'IRANSans_Light',
					fontSize: 15
				}
			});
			return;
		}
		this.setState({ onWorking: true });
		Keyboard.dismiss();
		let status = await Api.request('user/change', 'POST', {
			code: this.state.confirmation
		});

		if (status[0] == 200) {
			Toast.show('تغییرات شما با موفقیت در سیستم ثبت شد .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#2ecc71',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontSize: 15,
					fontFamily: 'IRANSans_Light'
				}
			});
			Actions.Settings();
		}
		this.setState({ onWorking: false });
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;
		return (
			<Container style={Styles.authWapper}>
				<Image
					source={require('../resource/images/bg_main.png')}
					style={[{ flex: 1, width: null, height: null }]}>
					<Content
						theme={barghtheme}
						keyboardShouldPersistTaps="always">
						<Header
							style={[
								{
									backgroundColor: 'rgba(255,255,255,0.1)',
									height: 60
								}
							]}>
							<Left>
								<Button
									onPress={() => {
										Actions.Profile({
											type: 'reset'
										});
									}}
									transparent
									iconLeft>
									<View style={[Styles.headerActionWrap]}>
										<Icon
											name="chevron-left"
											style={[Styles.headerActionIcon]}
										/>
										<Text
											style={[
												Styles.fontSans,
												Styles.headerActionText,
												{
													fontSize: 14,
													paddingLeft: 10
												}
											]}>
											بازگشت
										</Text>
									</View>
								</Button>
							</Left>
							<Right />
						</Header>

						<View>
							{!this.state.keyBoardShow
								? <View
										style={[
											{
												alignSelf: 'center',
												marginTop: height * 0.1
											}
										]}>
										<Image
											source={require('../resource/images/logo_main.png')}
											style={[{ width: 120, height: 136 }]}
										/>
									</View>
								: <View />}
							<View
								style={[
									{
										alignSelf: 'center',
										width: width * 0.8,
										marginTop: height * 0.1
									}
								]}>
								{this.state.type == 'email'
									? <Text
											style={[
												Styles.fontSans,
												{
													color: '#fff',
													alignSelf: 'center',
													textAlign: 'center',
													fontSize: 12
												}
											]}>
											برای تغییر آدرس ایمیل لطفا کدی که به
											آدرس ایمیل جدید شما ارسال شده است را
											در کادر زیر وارد کنید .
										</Text>
									: <Text
											style={[
												Styles.fontSans,
												{
													color: '#fff',
													alignSelf: 'center',
													textAlign: 'center',
													fontSize: 12
												}
											]}>
											برای تغییر شماره موبایل لطفا کدی که به
											شماره موبایل جدید شما ارسال شده است را
											در کادر زیر وارد کنید .
										</Text>}
							</View>
							<View
								style={[
									{
										alignSelf: 'center',
										width: width * 0.8,
										marginTop: height * 0.05
									}
								]}>
								<InputGroup
									iconRight
									style={[
										Styles.authInputs,
										{ alignSelf: 'center' }
									]}>
									<Input
										style={[
											Styles.fontSans,
											Styles.authInputText
										]}
										keyboardType="numeric"
										value={this.state.confirmation}
										placeholderTextColor="white"
										placeholder="کد دریافتی"
										onChangeText={text => {
											this.setState({
												confirmation: text
											});
										}}
									/>
									<Icon
										name="key"
										style={[
											Styles.authInputIcon,
											{ fontSize: 26 }
										]}
									/>
								</InputGroup>
							</View>
							{this.state.onWorking
								? <View
										style={[
											{
												alignSelf: 'center',
												alignItems: 'center',
												marginTop: 30,
												flexDirection: 'row'
											}
										]}>
										<Bubbles size={13} color="#FF4C74" />
									</View>
								: <Button
										onPress={() => {
											this.confirm();
										}}
										transparent
										style={[
											Styles.authButton,
											{
												alignSelf: 'center',
												marginTop: 30,
												flexDirection: 'row'
											}
										]}>
										<Text
											style={[
												Styles.fontSans,
												Styles.authButtonText,
												{ color: '#fff' }
											]}>
											بررسی کد دریافتی
										</Text>
									</Button>}
						</View>
					</Content>
				</Image>
			</Container>
		);
	}
}
