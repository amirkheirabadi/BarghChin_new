import React, {
    Component
} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
} from 'native-base';
import Products from './products';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import GiftedListView from 'react-native-gifted-listview';
import {
    Actions
} from 'react-native-router-flux';

import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal
} from 'react-native';
import Api from '../helper/api';
var styles = require('../style.js');
export default class CompanyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            modalVisible: false
        };
    }
    render() {
        const width = Dimensions.get('window').width
        const height = Dimensions.get('window').height
        return (<Container>
                <Content theme={barghtheme} style={[{
                    
                }]}>
                    <Grid>
                       <View style={[{backgroundColor:'#5c4b8c'}]}>
                         <View style={[{
                            position: 'absolute',
                            left:     0,
                            top:      0,
                        }]}>
                            <Image source={require('../resource/images/cover3.jpeg')} style={[{
                                height: 200
                            }
                        ]}/>
                        </View>

                        <View style={[{
                            alignItems:'center',
                            height:200,
                            marginBottom:50
                        }]}>
                            <Image source={require('../resource/images/cover2.jpeg')} style={[{
                                width: 100,
                                marginTop:135,
                                height: 100,
                                borderRadius:50,
                                borderColor: '#fff',
                                borderWidth: 2,
                                alignSelf: 'center',
                                }
                            ]}/>
                        </View>

                       </View>
                        <Row>
 
                        </Row>
                    </Grid>
                </Content>
            </Container>);
    }
}
