import React, { Component } from 'react';
import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Button,
	Row,
	Col,
	Picker,
	Item,
	Left,
	Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-root-toast';
import Loading from '../Loading';
import { Actions } from 'react-native-router-flux';
import {
	Text,
	View,
	Dimensions,
	TouchableWithoutFeedback,
	Modal,
	BackAndroid,
	Keyboard,
	Image
} from 'react-native';
import Api from '../helper/api';
var config = require('../config');
var Styles = require('../style.js');
var loading;
export default class Profile extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: 'ویرایش نام',
			first_name: '',
			last_name: '',
			email: '',
			mobile: '',
			inputField: '',
			inputValue: '',
			avatar: config['server'] + '/default_avatar.png',
			mewAvatar: '',

			oldPassword: '',
			newPassword: '',
			confirmPassword: '',

			modalVisible: false,
			imagePicker: {
				title: 'انتخاب تصویر',
				mediaType: 'photo',
				quality: 1,
				maxWidth: 960,
				maxHeight: 540,
				cancelButtonTitle: 'انصراف',
				takePhotoButtonTitle: 'گرفتن تصویر',
				chooseFromLibraryButtonTitle: 'انتخاب از گالری',
				storageOptions: {
					skipBackup: true,
					path: 'images'
				}
			}
		};
	}

	async imagePicker() {
		ImagePicker.showImagePicker(this.state.imagePicker, response => {
			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
				console.log(
					'User tapped custom button: ',
					response.customButton
				);
			} else {
				this.setState({ newAvatar: response.uri });
				this.UpdateAvatar();
			}
		});
	}

	async UpdateAvatar() {
		if (!this.state.newAvatar) {
			return false;
		}
		loading.show();
		let status = await Api.request(
			'user/profile',
			'POST',
			{ type: 'avatar' },
			{ avatar: this.state.newAvatar }
		);
		loading.hide();
		if (status[0] == 200) {
			console.log(status[1]);

			this.setState({
				avatar: status[1]['data']['avatar']
			});
			Toast.show('ویرایش آواتار با موفقیت انجام شد .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#2ecc71',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontFamily: 'IRANSans_Light',
					fontSize: 15
				}
			});
		}
	}

	async componentDidMount() {
		BackAndroid.addEventListener('hardwareBackPress', () => {
			Actions.Home();
			return true;
		});
		let user = await Api.userProfile();

		this.setState({
			first_name: user['first_name'],
			last_name: user['last_name'],
			email: user['email'],
			mobile: user['mobile'],
			avatar: user['avatar']
		});
		loading.hide();
	}

	edit(param) {
		trans = {
			first_name: 'نام',
			last_name: 'نام خانوادگی',
			email: 'ایمیل',
			mobile: 'موبایل'
		};

		this.setState({
			modalVisible: true,
			title: 'ویرایش ' + trans[param],
			inputField: param,
			inputValue: this.state[param]
		});
	}

	async confirm() {
		loading.show();
		Keyboard.dismiss();
		let status = await Api.request('user/profile', 'POST', {
			type: this.state.inputField,
			value: this.state.inputValue
		});
		loading.hide();
		if (status[0] == 200) {
			if (
				this.state.inputField == 'email' ||
				this.state.inputField == 'mobile'
			) {
				Actions.ProfileConfirm({
					data: { type: this.state.inputField }
				});
			}
			changes = {};
			changes[this.state.inputField] = this.state.inputValue;
			this.setState(changes);
		}

		this.setState({ modalVisible: false });
	}

	async changePassword() {
		if (!this.state.oldPassword) {
			Toast.show('لطفا رمز عبور کنونی خود را وارد کنید .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#e74c3c',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontFamily: 'IRANSans_Light'
				}
			});
			return;
		}
		Keyboard.dismiss();
		if (!this.state.newPassword || !this.state.confirmPassword) {
			Toast.show(
				'لطفا رمز عبور جدید خود به همراه تکرارش را وارد نمایید .',
				{
					duration: Toast.durations.LONG,
					position: -50,
					backgroundColor: '#e74c3c',
					shadow: true,
					animation: true,
					hideOnPress: true,
					delay: 0,
					textStyle: {
						color: '#fff',
						fontFamily: 'IRANSans_Light',
						fontSize: 15
					}
				}
			);
			return;
		}
		loading.show();
		let status = await Api.request('user/password', 'POST', {
			old_password: this.state.oldPassword,
			password: this.state.newPassword,
			password_confirmation: this.state.confirmPassword
		});
		loading.hide();
		if (status[0] == 200) {
			Toast.show('رمز عبور شما با موفقت تعویض شد .', {
				duration: Toast.durations.LONG,
				position: -50,
				backgroundColor: '#2ecc71',
				shadow: true,
				animation: true,
				hideOnPress: true,
				delay: 0,
				textStyle: {
					color: '#fff',
					fontFamily: 'IRANSans_Light',
					fontSize: 15
				}
			});
		}
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;
		return (
			<Container>
				<Header style={[Styles.HeaderWrapper]}>
					<Left>
						<Button
							onPress={() => {
								Actions.Home({ type: 'reset' });
							}}
							transparent>
							<View style={[Styles.headerActionWrap]}>
								<Icon
									name="chevron-left"
									style={[Styles.headerActionIcon]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.headerActionText
									]}>
									بازگشت
								</Text>
							</View>
						</Button>
					</Left>
					<Right>
						<Title
							style={[Styles.fontSans, Styles.headerTitleRight]}>
							پروفایل
						</Title>
					</Right>
				</Header>
				<Content
					theme={barghtheme}
					style={[Styles.mainWrapper]}
					keyboardShouldPersistTaps="always">
					<Loading default={true} ref={ref => (loading = ref)} />
					<Modal
						styles={{ flex: 1, backgroundColor: '#000' }}
						animationType={'fade'}
						transparent={true}
						visible={this.state.modalVisible}
						onRequestClose={() => {
							this.setState({ modalVisible: false });
						}}>
						<Container style={Styles.authModalWrapper}>
							<View
								style={{
									padding: 10,
									backgroundColor: '#fff',
									alignItems: 'flex-end',
									width: 300,
									height: 150
								}}>
								<Row>
									<Text
										style={[
											Styles.fontSans,
											{ fontSize: 13 }
										]}>
										{this.state.title}
									</Text>
								</Row>
								<Row
									style={{
										alignSelf: 'center',
										marginTop: 0
									}}>
									<InputGroup
										borderType="regular"
										style={[
											{
												borderRadius: 3,
												backgroundColor: '#ecf0f1',
												width: 280,
												height: 35
											}
										]}>
										<Input
											value={this.state.inputValue}
											onChangeText={text => {
												this.setState({
													inputValue: text
												});
											}}
										/>
									</InputGroup>
								</Row>
								<Row style={{ alignSelf: 'flex-start' }}>
									<Button
										onPress={() => {
											this.setState({
												modalVisible: false
											});
										}}
										transparent
										iconRight
										style={{
											padding: 0,
											margin: 0,
											width: 120,
											justifyContent: 'center'
										}}>
										<Text
											style={[
												Styles.fontSans,
												{
													fontSize: 11,
													paddingRight: 10
												}
											]}>
											انصراف
										</Text>
										<Icon name="close" />
									</Button>
									<Button
										onPress={() => {
											this.confirm();
										}}
										transparent
										iconRight
										style={{
											padding: 0,
											margin: 0,
											width: 120,
											marginLeft: 40,
											justifyContent: 'center'
										}}>
										<Text
											style={[
												Styles.fontSans,
												{
													fontSize: 11,
													paddingRight: 10
												}
											]}>
											تایید
										</Text>
										<Icon name="check" />
									</Button>
								</Row>
							</View>
						</Container>
					</Modal>

					<View style={[Styles.alertInfo, { marginBottom: 10 }]}>
						<Row>
							<Col>
								<Button
									small
									onPress={() => {
										Actions.Plan({ type: 'reset' });
									}}
									info
									style={{ marginTop: 5 }}>
									<Text
										style={[
											Styles.fontSans,
											{ color: '#fff' }
										]}>
										خرید اشتراک
									</Text>
								</Button>
							</Col>
							<Col>
								<Text
									style={[
										Styles.fontSansLight,
										Styles.alertInfoText
									]}>
									با خرید اشتراک بدون محدودیت به داده ها
									دسترسی داشته باشید .
								</Text>
							</Col>
						</Row>
					</View>

					<View style={[Styles.blockHeader]}>
						<Text style={[Styles.fontSans, Styles.blockHeaderText]}>
							اطلاعات کاربری
						</Text>
					</View>
					<View style={[Styles.innerContainer, { marginTop: 0 }]}>
						<Row>
							<Col>
								<TouchableWithoutFeedback
									onPress={() => {
										this.edit('last_name');
									}}>
									<View>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#5c4b8c',
													fontSize: 12
												}
											]}>
											نام خانوادگی
										</Text>
										<Text
											style={[
												Styles.fontSans,
												{
													fontSize: 13,
													textAlign: 'right'
												}
											]}>
											{this.state.last_name}
										</Text>
									</View>
								</TouchableWithoutFeedback>
							</Col>
							<Col style={{ width: width * 0.3 }}>
								<TouchableWithoutFeedback
									onPress={() => {
										this.edit('first_name');
									}}>
									<View>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#5c4b8c',
													fontSize: 12
												}
											]}>
											نام
										</Text>
										<Text
											style={[
												Styles.fontSans,
												{
													fontSize: 13,
													textAlign: 'right'
												}
											]}>
											{this.state.first_name}
										</Text>
									</View>
								</TouchableWithoutFeedback>
							</Col>
						</Row>

						<Row style={{ marginTop: 20 }}>
							<Col>
								<TouchableWithoutFeedback
									onPress={() => {
										this.edit('email');
									}}>
									<View>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#5c4b8c',
													fontSize: 12,
													textAlign: 'right'
												}
											]}>
											پست الکترونیک
										</Text>
										<Text style={[Styles.fontSans]}>
											{this.state.email}
										</Text>
									</View>
								</TouchableWithoutFeedback>
							</Col>

							<Col style={{ width: width * 0.3 }}>
								<TouchableWithoutFeedback
									onPress={() => {
										this.edit('mobile');
									}}>
									<View>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#5c4b8c',
													fontSize: 12,
													textAlign: 'right'
												}
											]}>
											شماره موبایل
										</Text>
										<Text style={[Styles.fontSans]}>
											{this.state.mobile}
										</Text>
									</View>
								</TouchableWithoutFeedback>
							</Col>
						</Row>
					</View>
					<TouchableWithoutFeedback
						onPress={() => {
							this.imagePicker();
						}}>
						<View style={[Styles.profileAvatarWrapper]}>
							<Image
								source={{ uri: this.state.avatar }}
								style={[Styles.profileAvatar]}
							/>
						</View>
					</TouchableWithoutFeedback>
					<View style={[Styles.blockHeader]}>
						<Text style={[Styles.fontSans, Styles.blockHeaderText]}>
							تغییر رمز عبور
						</Text>
					</View>
					<View style={[Styles.innerContainer, { marginTop: 0 }]}>
						<Row>
							<Col>
								<View>
									<Text
										style={[
											Styles.fontSans,
											{ color: '#5c4b8c', fontSize: 12 }
										]}>
										رمز عبور فعلی
									</Text>
									<Input
										style={[Styles.fontSans]}
										secureTextEntry={true}
										placeholder="******"
										onChangeText={text => {
											this.setState({
												oldPassword: text
											});
										}}
									/>
								</View>
							</Col>
						</Row>

						<Row>
							<Col>
								<View>
									<Text
										style={[
											Styles.fontSans,
											{ color: '#5c4b8c', fontSize: 12 }
										]}>
										تکرار رمز عبور
									</Text>
									<Input
										style={[Styles.fontSans]}
										secureTextEntry={true}
										placeholder="******"
										onChangeText={text => {
											this.setState({
												newPassword: text
											});
										}}
									/>
								</View>
							</Col>
							<Col>
								<View>
									<Text
										style={[
											Styles.fontSans,
											{ color: '#5c4b8c', fontSize: 12 }
										]}>
										رمز عبور جدید
									</Text>
									<Input
										style={[Styles.fontSans]}
										secureTextEntry={true}
										placeholder="******"
										onChangeText={text => {
											this.setState({
												confirmPassword: text
											});
										}}
									/>
								</View>
							</Col>
						</Row>
					</View>
					<View style={[Styles.blockHeader]}>
						<Text style={[Styles.fontSans, Styles.blockHeaderText]}>
							مدیریت کسب و کارها
						</Text>
					</View>
					<View
						style={{
							width: width - 40,
							alignSelf: 'center',
							paddingBottom: 20
						}}>
						<Row>
							<View
								style={[
									Styles.profileBussinessItem,
									{ marginLeft: 0 }
								]}>
								<Image
									source={require('../resource/icon/company.png')}
									style={[Styles.profileBussinessImage]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.profileBussinessText
									]}>
									شرکت / فروشنده
								</Text>
							</View>

							<View style={[Styles.profileBussinessItem]}>
								<Image
									source={require('../resource/icon/producer.png')}
									style={[Styles.profileBussinessImage]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.profileBussinessText
									]}>
									تولید کننده
								</Text>
							</View>

							<View style={[Styles.profileBussinessItem]}>
								<Image
									source={require('../resource/icon/importer.png')}
									style={[Styles.profileBussinessImage]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.profileBussinessText
									]}>
									وارد کننده
								</Text>
							</View>

							<View
								style={[
									Styles.profileBussinessItem,
									{ marginRight: 0 }
								]}>
								<Image
									source={require('../resource/icon/technician.png')}
									style={[Styles.profileBussinessImage]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.profileBussinessText
									]}>
									تکنیسن
								</Text>
							</View>
						</Row>
					</View>
				</Content>
			</Container>
		);
	}
}
