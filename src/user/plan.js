import React, { Component } from 'react';
import {
	Container,
	Content,
	InputGroup,
	Input,
	Header,
	Title,
	Button,
	Row,
	Col,
	Picker,
	Item,
	Left,
	Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Icon from 'react-native-vector-icons/FontAwesome';
import Toast from 'react-native-root-toast';
import { Actions } from 'react-native-router-flux';
import {
	Text,
	View,
	Dimensions,
	TouchableWithoutFeedback,
	Modal,
	BackAndroid,
	Keyboard,
	Image
} from 'react-native';
import Loading from '../Loading';
var loading;
import Api from '../helper/api';
var Styles = require('../style.js');
export default class Plan extends Component {
	constructor(props) {
		super(props);

		this.state = {
			premium: false,
			expireDate: '',
			plans: [],
			buttonText: 'خرید',
			modalVisible: false,
			selectedPlan: 0
		};
	}

	async componentDidMount() {
		if (this.props.pay) {
			switch (this.props.pay) {
				case 'success':
					Toast.show('پرداخت شما با موفقیت انجام شد .', {
						duration: Toast.durations.LONG,
						position: -50,
						backgroundColor: '#2ecc71',
						shadow: true,
						animation: true,
						hideOnPress: true,
						delay: 0,
						textStyle: {
							color: '#fff',
							fontFamily: 'IRANSans_Light',
							fontSize: 15
						}
					});
					break;

				case 'failed':
					Toast.show('خطا در عملیات پرداخت .', {
						duration: Toast.durations.LONG,
						position: -50,
						backgroundColor: '#e74c3c',
						shadow: true,
						animation: true,
						hideOnPress: true,
						delay: 0,
						textStyle: {
							color: '#fff',
							fontFamily: 'IRANSans_Light',
							fontSize: 15
						}
					});
					break;
			}
		}

		BackAndroid.addEventListener('hardwareBackPress', () => {
			Actions.Profile();
			return true;
		});
		let user = await Api.userProfile();
		loading.hide();
		this.setState({
			plans: user['plans'],
			expireDate: user['user']['expire_premium']['ago'],
			premium: user['user']['premium'],
			buttonText: user['user']['premium'] ? 'تمدید' : 'خرید'
		});
	}

	planPay = async () => {
		this.setState({
			modalVisible: false
		});
		loading.show();
		let status = await Api.request(
			'plan/subscribe/' + this.state.selectedPlan,
			'POST'
		);
		loading.hide();
		if (status[0] == 200) {
			Actions.PlanPay({
				url: status[1]['data']['url']
			});
		}
	};

	buyPlan(id) {
		this.setState({
			selectedPlan: id,
			modalVisible: true
		});
	}

	render() {
		const width = Dimensions.get('window').width;
		const height = Dimensions.get('window').height;

		const plansItems = this.state.plans.map(item => {
			return (
				<Item
					style={[Styles.fontSans, { color: 'red' }]}
					key={item.id}
					label={item.name + '     روز ' + item.duration}
					value={item.id}
				/>
			);
		});
		return (
			<Container>
				<Header style={[Styles.HeaderWrapper, { elevation: 0 }]}>
					<Left>
						<Button
							onPress={() => {
								Actions.Home({ type: 'reset' });
							}}
							transparent>
							<View style={[Styles.headerActionWrap]}>
								<Icon
									name="chevron-left"
									style={[Styles.headerActionIcon]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.headerActionText
									]}>
									بازگشت
								</Text>
							</View>
						</Button>
					</Left>
					<Right />
				</Header>
				<Modal
					styles={{ flex: 1, backgroundColor: '#000' }}
					animationType={'fade'}
					transparent={true}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						this.setState({ modalVisible: false });
					}}>
					<Container style={Styles.authModalWrapper}>
						<View
							style={{
								padding: 10,
								backgroundColor: '#fff',
								alignItems: 'flex-end',
								width: 300,
								height: 150
							}}>
							<Row>
								<Text
									style={[Styles.fontSans, { fontSize: 13 }]}>
									{this.state.title}
								</Text>
							</Row>
							<Row style={{ alignSelf: 'center', marginTop: 0 }}>
								<Text style={[Styles.fontSans]}>
									آیا از خرید این پلن افزایشی مطمئن هستید ؟
								</Text>
							</Row>
							<Row style={{ alignSelf: 'flex-start' }}>
								<Button
									onPress={() => {
										this.setState({
											modalVisible: false
										});
									}}
									transparent
									iconRight
									style={{
										padding: 0,
										margin: 0,
										width: 120,
										justifyContent: 'center'
									}}>
									<Text
										style={[
											Styles.fontSans,
											{ fontSize: 11, paddingRight: 10 }
										]}>
										انصراف
									</Text>
									<Icon name="close" />
								</Button>
								<Button
									onPress={this.planPay}
									transparent
									iconRight
									style={{
										padding: 0,
										margin: 0,
										width: 120,
										marginLeft: 40,
										justifyContent: 'center'
									}}>
									<Text
										style={[
											Styles.fontSans,
											{ fontSize: 11, paddingRight: 10 }
										]}>
										تایید
									</Text>
									<Icon name="check" />
								</Button>
							</Row>
						</View>
					</Container>
				</Modal>
				<Content
					theme={barghtheme}
					style={[Styles.mainWrapper]}
					keyboardShouldPersistTaps="always">
					<Loading default={true} ref={ref => (loading = ref)} />
					<View
						style={[{ backgroundColor: '#5c4b8c', height: 80 }]}
					/>

					<View
						style={[
							Styles.innerContainer,
							{ marginTop: -40, marginBottom: 20 }
						]}>
						<View
							style={{
								marginBottom: 10,
								paddingBottom: 10,
								borderBottomWidth: 0.2,
								borderBottomColor: '#95a5a6'
							}}>
							{this.state.premium
								? <View style={{ alignSelf: 'center' }}>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#2ecc71',
													fontSize: 13,
													elevation: 8,
													textAlign: 'center'
												}
											]}>
											اشتراک زمانی در مدت{' '}
											{this.state.expireDate} دیگر به پایان
											می رسد .
										</Text>
									</View>
								: <View style={{ alignSelf: 'center' }}>
										<Text
											style={[
												Styles.fontSans,
												{
													color: '#e74c3c',
													fontSize: 13,
													elevation: 8,
													textAlign: 'center'
												}
											]}>
											شما هیچ اشتراک زمانی ندارید و یا
											اشتراک زمانی شما به پایان رسیده است .
										</Text>
									</View>}
						</View>

						<Text style={[Styles.fontSansLight]}>
							با خرید اشتراک زمانی می توانید به راحتی به اطلاعات
							کسب و کارهای مختلف دسترسی پیدا کنید و بدون محدودیت
							از تمامی اطلاعات برق چین استفاده کنید
						</Text>
					</View>

					{this.state.plans.map(item => {
						return (
							<View
								style={[
									Styles.innerContainer,
									{ marginVertical: 10 }
								]}>
								<Row>
									<Col>
										<Button
											onPress={() => {
												this.buyPlan(item.id);
											}}
											success
											style={{
												borderRadius: 4,
												elevation: 8
											}}>
											<Text
												style={[
													Styles.fontSans,
													{ color: '#fff' }
												]}>
												{this.state.buttonText} اشتراک
											</Text>
										</Button>
									</Col>
									<Col style={{ paddingTop: 12 }}>
										<Text
											style={[
												Styles.fontSans,
												{
													textAlign: 'center',
													color: '#2ecc71'
												}
											]}>
											{item.price / 10} تومان
										</Text>
									</Col>
									<Col>
										<Text style={[Styles.fontSans]}>
											{item.name}
										</Text>
										<Text
											style={[
												Styles.fontSansLight,
												{ fontSize: 13 }
											]}>
											مدت زمان اشتراک : {item.duration}{' '}
											روز
										</Text>
									</Col>
								</Row>
							</View>
						);
					})}
				</Content>
			</Container>
		);
	}
}
