import React, { Component } from 'react';
import {
	Container,
	Content,
	Input,
	Label,
	Header,
	Title,
	Button,
	Form,
	Item,
	Left,
	Right,
	Col,
	Row
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import barghtheme from '../theme/barghchin';
import GiftedListView from 'react-native-gifted-listview';
import { Actions } from 'react-native-router-flux';
import {
	Image,
	Text,
	View,
	Dimensions,
	Animated,
	TouchableHighlight,
	TouchableWithoutFeedback,
	ScrollView,
	Keyboard
} from 'react-native';
import Api from '../helper/api';

var Styles = require('../style.js');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class Articles extends Component {
	constructor(props) {
		super(props);

		this.state = { search: '', searchText: '', forceUpdate: false };
	}

	_onFetch = async (page = 1, callback, options) => {
		let response = await Api.request(
			'articles?page=' + page + '&search=' + this.state.search,
			'GET'
		);
		if (response[0] == 200) {
			return callback([response[1]['data']]);
		}
	};

	_renderRowView(rowData) {
		return (
			<View>
				{rowData.length
					? <View style={[Styles.innerContainer]}>
							{rowData.map((s, i) => {
								return (
									<View>
										<TouchableWithoutFeedback
											onPress={() =>
												Actions.ArticleItem({
													id: s.id,
													title: s.title,
													role: s.role
												})}>
											<View style={[Styles.newsItem]}>
												<View style={[Styles.newIntro]}>
													<Text
														style={[
															Styles.fontSans,
															{ paddingBottom: 10 }
														]}>
														{s['title']}
													</Text>
													<Text
														style={[
															Styles.fontSansLight,
															{
																fontSize: 11,
																paddingBottom: 5
															}
														]}>
														نویسنده : ( {s.role}
														) {s.writter}
													</Text>
													<Text
														style={[
															Styles.fontSansLight,
															{ fontSize: 11 }
														]}>
														تاریخ ارسال :{' '}
														{s.created_at}
													</Text>
												</View>
												<Image
													resizeMode={'cover'}
													source={{ uri: s['thumb'] }}
													style={[Styles.newsCover]}
												/>
											</View>
										</TouchableWithoutFeedback>
									</View>
								);
							})}
						</View>
					: <View />}
			</View>
		);
	}

	_renderPaginationWaitingView(paginateCallback) {
		return (
			<View
				style={{
					marginVertical: 30
				}}>
				<TouchableWithoutFeedback onPress={paginateCallback}>
					<View style={[Styles.loadMore]}>
						<Text
							style={[Styles.fontSansLight, Styles.loadMoreText]}>
							نمایش بیشتر
						</Text>
					</View>
				</TouchableWithoutFeedback>
			</View>
		);
	}

	_emptyDisplay() {
		return (
			<View
				style={[
					{
						marginTop: 10,
						padding: 10,
						alignSelf: 'center'
					}
				]}>
				<Text
					style={[
						Styles.fontSans,
						{
							textAlign: 'center'
						}
					]}>
					نتیجه ای برای نمایش وجود ندارد .
				</Text>
			</View>
		);
	}

	render() {
		return (
			<Container>
				<Header style={[Styles.HeaderWrapper]}>
					<Left>
						<Button
							onPress={() => {
								Actions.Home({ type: 'reset' });
							}}
							transparent>
							<View style={[Styles.headerActionWrap]}>
								<Icon
									name="chevron-left"
									style={[Styles.headerActionIcon]}
								/>
								<Text
									style={[
										Styles.fontSans,
										Styles.headerActionText
									]}>
									بازگشت
								</Text>
							</View>
						</Button>
					</Left>
					<Right>
						<Title
							style={[Styles.fontSans, Styles.headerTitleRight]}>
							مقالات
						</Title>
					</Right>
				</Header>
				<Content
					theme={barghtheme}
					style={[Styles.mainWrapper]}
					keyboardShouldPersistTaps="always">
					<View style={[Styles.innerContainer]}>
						<Row>
							<Col style={{ width: 50 }}>
								<Button
									info
									style={[Styles.searchButton]}
									onPress={() => {
										setTimeout(() => {
											this.setState({
												forceUpdate: true,
												search: this.state.searchText
											});
											Keyboard.dismiss();
										}, 10);
									}}>
									<Icon
										name="search"
										style={[Styles.searchIcon]}
									/>
								</Button>
							</Col>
							<Col>
								<Item>
									<Input
										placeholder="جستجو در مقالات"
										onChangeText={text => {
											this.setState({ searchText: text });
										}}
									/>
								</Item>
							</Col>
						</Row>
					</View>
					<GiftedListView
						enableEmptySections={true}
						ref="listview"
						emptyView={this._emptyDisplay}
						rowView={this._renderRowView.bind(this)}
						onFetch={this._onFetch}
						firstLoader={true}
						refreshable={false}
						pagination={true}
						forceUpdate={this.state.forceUpdate}
						withSections={false}
						customStyles={{
							paginationView: { backgroundColor: '#eee' }
						}}
						refreshableTintColor="blue"
						paginationWaitingView={
							this._renderPaginationWaitingView
						}
					/>
				</Content>
			</Container>
		);
	}
}
