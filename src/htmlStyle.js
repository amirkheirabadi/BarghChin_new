import {StyleSheet, Dimensions} from 'react-native';
const width = Dimensions
    .get('window')
    .width;
const height = Dimensions
    .get('window')
    .height;
module.exports = StyleSheet.create({
    p : {
        fontFamily: 'IRANSans_Light',
    },
    span : {
        fontFamily: 'IRANSans_Light'
    },
    html : {
        fontFamily: 'IRANSans_Light'
    },
    h1 : {
        fontFamily: 'IRANSans_Light'
    },
        h2 : {
        fontFamily: 'IRANSans_Light'
    },
        h3 : {
        fontFamily: 'IRANSans_Light'
    },
        h4 : {
        fontFamily: 'IRANSans_Light'
    },
        h5 : {
        fontFamily: 'IRANSans_Light'
    },
        h6 : {
        fontFamily: 'IRANSans_Light'
    },
        h7 : {
        fontFamily: 'IRANSans_Light'
    },
        h8 : {
        fontFamily: 'IRANSans_Light'
    },
    bold: {
        fontWeight: 'bold',
    }
});
