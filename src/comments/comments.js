import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Toast from 'react-native-root-toast';
import StarRating from 'react-native-star-rating';
import GiftedListView from 'react-native-gifted-listview';
import {Actions} from 'react-native-router-flux';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker
} from 'react-native';
import Api from '../helper/api';

var styles = require('../style.js');
export default class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entity_alias: {
                'company': 'پاسخ فروشنده',
                'importer': 'پاسخ وارد کننده',
                'produter': 'پاسخ تولید کننده',
                'technicians': 'پاسخ تکنسین',
                'product': 'پاسخ فروشنده'
            },
            entity: '',
            id: '',

            comment_count: 0,
            rating: '',
            message: ''
        };
    }

    componentDidMount() {
        // if (!this.props.entity || !this.props.id) {
        //     return Actions.pop();
        // }
        this.setState({
            entity: 'product', //this.props.entity,
            id: 1 //this.props.id
        });
    }

    async _onFetch(page = 1,
        callback,
        options
    ) {
        let response = await Api.request('comments',
            'POST', {
            entity: 'product', //this.state.entity
            id: 1,
            page: page
        }
        );
        if (response[0] == 200) {
            this.setState({'comment_count': response[1]['data']['comment_count']
            });
            return callback(response[1]['data']['comments']);
        }
    }

    _renderRowView(rowData) {
        return (
            <View style={{
                    padding: 10,
                    borderBottomWidth: 0.3,
                    borderColor: '#C5CACB'
                }}>
                <Row>
                    <Col>
                        <Row>
                            <Text style={[
                                    styles.fontSansLight, {
                                        fontSize: 10
                                    }
                                ]}>{rowData.created.time}</Text>
                        </Row>
                        <Row>
                            <StarRating style={{
                                    width: 100
                                }} disabled={true} maxStars={5} rating={rowData.rating} starSize={12} starColor="#f1c40f" emptyStarColor="#f1c40f"/>
                        </Row>
                    </Col>
                    <Col style={{
                            alignItems: 'flex-end'
                        }}>
                        <Row>
                            <Text style={[
                                    styles.fontSans, {
                                        paddingRight: 15,
                                        paddingTop: 17,
                                        fontSize: 13
                                    }
                                ]}>{
                                    rowData.sender.first_name +
                                            ' ' +
                                            rowData.sender.last_name
                                }</Text>
                            <Image resizeMode="stretch" source={{
                                    uri: rowData.sender.avatar
                                }} style={[{
                                        width: 50,
                                        height: 50
                                    }
                                ]}/>
                        </Row>
                    </Col>
                </Row>
                <Row style={{
                        justifyContent: 'flex-end',
                        paddingLeft: 10,
                        paddingRight: 70
                    }}>
                    <Text style={[
                            styles.fontSansLight, {
                                fontSize: 12,
                                alignSelf: 'flex-end'
                            }
                        ]}>{rowData.text}</Text>
                </Row>

                {
                    rowData.answer
                        ? (
                            <View style={[{
                                        backgroundColor: '#C5CACB',
                                        padding: 10,
                                        marginRight: 70,
                                        marginTop: 5
                                    }
                                ]}>
                                <Row>
                                    <Col style={{
                                            alignItems: 'flex-start'
                                        }}>
                                        <Text style={[
                                                styles.fontSansLight, {
                                                    fontSize: 12
                                                }
                                            ]}>{rowData.answer_at.time}</Text>
                                    </Col>
                                    <Col style={{
                                            alignItems: 'flex-end'
                                        }}>
                                        <Row>
                                            <Text style={[
                                                    styles.fontSans, {
                                                        fontSize: 12
                                                    }
                                                ]}>{
                                                    this
                                                        .state
                                                        .entity_alias[this.state.entity]
                                                }</Text>
                                            <Icon name='chevron-left'  style={{fontSize:16, paddingTop: 3}}/>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row style={{
                                        justifyContent: 'flex-end',
                                        paddingRight: 10,
                                        paddingLeft: 10
                                    }}>
                                    <Text style={[
                                            styles.fontSansLight, {
                                                fontSize: 12
                                            }
                                        ]}>{rowData.answer}</Text>
                                </Row>
                            </View>
                        )
                        : (
                            <View></View>
                        )
                }
            </View>
        );
    }

    _onPress(rowData) {
        this.setState({name: rowData.name});
    }

    _refresh() {
        this
            .refs
            .listview
            ._refresh();
    }

    _emptyDisplay() {
        return (
            <View style={[{
                        marginTop: 10,
                        padding: 10,
                        alignSelf: 'center'
                    }
                ]}>
                <Text style={[
                        styles.fontSans, {
                            textAlign: 'center'
                        }
                    ]}>
                    نتیجه ای برای نمایش وجود ندارد.
                </Text>
                <Text style={[
                        styles.fontSans, {
                            textAlign: 'center',
                            marginTop: 15
                        }
                    ]}>
                    اگر دسته بندی مد نظر شماست که در لیست برق چین وجود ندارد لطفا از طریق دکمه زیر آن را به ما اطلاع دهید.
                </Text>
                <Image resizeMode="stretch" source={require('../resource/images/arrow.png')} style={[{
                            marginTop: 30,
                            width: 80,
                            height: 120,
                            alignSelf: 'center'
                        }
                    ]}/>
            </View>
        );
    }

    _paginationFetchigView (paginateCallback){
        return (
           <Row style={{justifyContent: 'center',alignItems: 'center',height:40,paddingTop:20}}>
             <TouchableWithoutFeedback
                onPress={paginateCallback}
            >
                <Text style={[styles.fontSans, {fontSize: 14}]}>
                    بارگزاری بیشتر
                </Text>
            </TouchableWithoutFeedback>
           </Row>
        );
    }

    new() {
        this.setState({name: this.state.newName});
        this.showModal(false);
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                <Content theme={barghtheme}>
                    <Header style={[{
                                flexDirection: 'row',
                                height: 50
                            }
                        ]}>
                        <Button onPress={() => {
                                Actions.pop();
                            }} iconLeft="iconLeft" transparent="transparent">
                            <Icon name='chevron-left'/>
                            <Text style={[styles.fontSans]}>بازگشت</Text>
                        </Button>
                        <Title style={[
                                styles.fontSansLight, {
                                    color: '#fff',
                                    textAlign: 'center',
                                    alignSelf: 'flex-end',
                                    fontSize: 14
                                }
                            ]}>دیدگاه کاربران ( {this.state.comment_count}
                            )</Title>
                    </Header>
                    <View style={{
                            flex: 1,
                            flexDirection: 'column',
                        }}>
                        <View style={[{
                            height: (height -195)
                        }]}>
                            <GiftedListView enableEmptySections={true} ref="listview" emptyView={this._emptyDisplay} rowView={this
                                    ._renderRowView
                                    .bind(this)} onFetch={this
                                    ._onFetch
                                    .bind(this)} firstLoader={true} pagination={true} withSections={false} customStyles={{
                                    paginationView: {
                                        backgroundColor: '#eee'
                                    }
                                }} refreshableTintColor="blue"
                                paginationWaitingView={this._paginationFetchigView} />
                        </View>

                        <Card style={[{
                                    width: width * 0.97,
                                    height: 120,
                                    alignSelf: 'center',
                                    backgroundColor: 'rgb(245, 245, 245)'
                                }
                            ]}>
                            <Row style={{justifyContent: 'center',alignItems: 'center',}}>
                                <View style={{width:150,alignSelfL:'center',paddingBottom:5}}>
                                    <StarRating selectedStar={(rating) => this.setState({rating: rating})} disabled={false} maxStars={5} rating={this.state.rating} starSize={20} starColor="#f1c40f"/>
                                </View>
                            </Row>
                            <Row>
                                <Col style={{width:width*0.2,alignItems:'center'}}>
                                    <TouchableWithoutFeedback>
                                        <View>
                                            <Icon name='message'/>
                                            <Text style={[styles.fontSans,{}]}>
                                                ارسال دیدگاه
                                            </Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </Col>

                                <Col style={{width:width*0.7}}>
                                    <InputGroup borderType='regular' style={[{
                                                borderWidth:0                                               
                                            }
                                        ]}>
                                        <Input multiline={true} placeholder='دیدگاه شما' value={this.state.price} onChangeText={(text) => {
                                                this.setState({price: text});
                                            }}/>
                                    </InputGroup>
                                </Col>
                            </Row>
                        </Card>
                    </View>
                </Content>
            </Container >
        );
    }
}
