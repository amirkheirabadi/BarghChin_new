import React, {Component} from 'react';
import {
    Container,
    Content,
    Button,
    Header,
    Title,
    Left,
    Right
} from 'native-base';
import barghtheme from '../theme/barghchin';
import Icon from "react-native-vector-icons/FontAwesome";
import {Actions} from 'react-native-router-flux';
import {
    Text,
    Dimensions,
    WebView,
    ScrollView,
    BackAndroid,
    View
} from 'react-native';
var config = require('../config.js');
var Styles = require('../style.js');
export default class Rules extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress',
            () => {
            Actions.pop();
            return true;
        }
        );
    }

    onNavigationStateChange(event) {
        if (event.title) {
            const htmlHeight = Number(event.title) //convert to number
            this.setState({Height:htmlHeight});
        }
    }

    redirect() {
        return Actions.pop();
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <Container>
                    <Header style={[Styles.HeaderWrapper]}>
                    <Left>
                        <Button
                        onPress={() => {
                            Actions.pop({ type: "reset" });
                        }}
                        transparent
                        >
                        <View style={[Styles.headerActionWrap]}>
                            <Icon name="chevron-left" style={[Styles.headerActionIcon]} />
                            <Text style={[Styles.fontSans, Styles.headerActionText]}>
                            بازگشت
                            </Text>
                        </View>
                        </Button>
                    </Left>
                    <Right>
                        <Title style={[Styles.fontSans, Styles.headerTitleRight]}>
                        قوانین
                        </Title>
                    </Right>
                    </Header>

                    <Content theme={barghtheme}>
                        <View style={[Styles.innerContainer]}>
                    <WebView 
                        scrollEnabled={false}
                        javaScriptEnabled ={true}
                        scrollEnabled={false}
                        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                        style={[{
                        flex: 1,
                        height:this.state.Height
                            }
                        ]} 
                        source={{
                            uri: config.server +
                                    '/api/page/rule'
                        }}
                    />
                    </View>
                </Content>
            </Container>
        );
    }
}
