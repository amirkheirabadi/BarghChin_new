import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    List,
    ListItem,
    Item,
    Button,
    Grid,
    Row
} from 'native-base';
import Carousel from 'react-native-looped-carousel';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView,
    Linking
} from 'react-native';
export default class SliderModule extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {}

    openAds = (url) => {
        Linking
            .openURL(url)
            .catch(err => console.error('An error occurred',
                err
            ));
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <View>
                <Carousel delay={5000} style={{
                        width: width,
                        height: 300
                    }} autoplay="autoplay" bullets={true} onAnimateNextPage={(p) => console.log(p)}>
                    {
                        this
                            .props
                            .data
                            .map((s,
                                i
                            ) => {
                                return <TouchableWithoutFeedback onPress={() => {
                                        this.openAds(s.url)
                                    }}>
                                    <View style={[{
                                                width: width,
                                                height: 300
                                            }
                                        ]}>
                                        <Image resizeMode={"cover"} source={{
                                                uri: s.picture
                                            }} style={[{
                                                    height: 300,
                                                    width: width,
                                                    alignSelf: 'stretch'
                                                }
                                            ]}/>
                                    </View>
                                </TouchableWithoutFeedback>
                            })
                    }
                </Carousel>
            </View>
        );
    }
}
