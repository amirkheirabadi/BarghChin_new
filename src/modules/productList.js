import React, {Component} from 'react';
import {
    Container,
    Content,
    InputGroup,
    Input,
    Icon,
    Header,
    Title,
    Button,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Card,
    CardItem,
    Fab,
    Body
} from 'native-base';
import Carousel from 'react-native-looped-carousel';
import {
    Image,
    Text,
    View,
    Dimensions,
    Animated,
    TouchableWithoutFeedback,
    Modal,
    Picker,
    ScrollView,
    Linking
} from 'react-native';
import GridView from 'react-native-super-grid';

var Styles = require('../style.js');

export default class ProductList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const width = Dimensions
            .get('window')
            .width
        const height = Dimensions
            .get('window')
            .height
        return (
            <View style={[]}>
                <Text style={[Styles.fontSans, Styles.moduleHeader]}>{this.props.title}</Text>
                    <GridView
                        style={[{width: width}]}
                        itemWidth={(width / 2) - 30}
                        items={this.props.data}
                        renderItem={s => (
                            <View style={[{
                                        marginHorizontal: 10,
                                        width: (width / 2) - 30
                                    }]}>
                                    <View style={[Styles.simpleBlock]}>
                                            <Image source={require('../resource/images/cover.jpeg')} style={[Styles.module_image_sample]}/>
                                            <View style={{
                                                    padding: 10
                                                }}>
                                                <Row style={[{
                                                            justifyContent: 'flex-end'
                                                        }
                                                    ]}>
                                                    <Text style={[
                                                            Styles.fontSans, {
                                                                fontSize: 13
                                                            }
                                                        ]}>
                                                        {s.name}
                                                    </Text>
                                                </Row>
                                                <Row style={{
                                                        marginTop: 3
                                                    }}>
                                                    <Col>
                                                        <Row>
                                                            <Icon name="remove-red-eye" style={[{
                                                                        color: '#6d8093',
                                                                        fontSize: 15
                                                                    }
                                                                ]}></Icon>
                                                            <Text style={[
                                                                    Styles.fontSansLight, {
                                                                        fontSize: 11,
                                                                        color: '#6d8093',
                                                                        paddingLeft: 5
                                                                    }
                                                                ]}>
                                                                {s.view}
                                                            </Text>
                                                        </Row>
                                                    </Col>
                                                    <Col>
                                                        <Text style={[
                                                                Styles.fontSans, {
                                                                    fontSize: 11
                                                                }
                                                            ]}>
                                                            {s.price}
                                                            ریال
                                                        </Text>
                                                    </Col>
                                                </Row>
                                            </View>
                                        </View>
                                        </View>
                        )}
                    />
            </View>
        );
    }
}
